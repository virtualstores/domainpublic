//
//  ShopApiMock.swift
//  Domain
//
//  Created by Carl-Johan Dahlman on 2019-10-23.
//

import Foundation
@testable import Domain

public class ShopApiMock: ShopApi {
    
    public var session: Session = Session(id: 42)
    public static var customerName: String = "Mock"
    public static var referenceCode: String = "referenceCode"
    public static var validIdentificationNumber: String = "123456789"
    public static var invalidIdentificationNumber: String = "qwerty"
    
    public static var barcode1: String = "123"
    public static var barcode2: String = "231"
    public static var barcode3: String = "312"
    
    public var item1: CartItem = CartItem(id: ShopApiMock.barcode1, parentId: 2, totalPrice: 10.9, article: nil, quantity: CartItemQuantity(deliveredNow: nil, quantity: 1, unitOfMeasure: "st"), vat: nil, discountContributions: nil, discountDetails: nil, depositInfo: nil)
    public var item2: CartItem = CartItem(id: ShopApiMock.barcode2, parentId: 2, totalPrice: 10.9, article: nil, quantity: CartItemQuantity(deliveredNow: nil, quantity: 1, unitOfMeasure: "st"), vat: nil, discountContributions: nil, discountDetails: nil, depositInfo: nil)
    public var item3: CartItem = CartItem(id: ShopApiMock.barcode3, parentId: 2, totalPrice: 10.9, article: nil, quantity: CartItemQuantity(deliveredNow: nil, quantity: 1, unitOfMeasure: "st"), vat: nil, discountContributions: nil, discountDetails: nil, depositInfo: nil)
    
    public var register: [String:CartItem]
    
    init() {
        register = [ item1.id: item1, item2.id: item2, item3.id: item3]
    }
    
    
    public var carts: [String: Cart] = [:]
    
    private func createNewCart() -> Cart {
        return Cart(id: "\(Int64.random(in: Range(uncheckedBounds: (lower: 10, upper: 10000))))", state: Cart.State.active, items: [], totals: CartTotals(discountAmount: 0, deliveredNowCartItemsTotalAmount: 0, saletotalAmount: 0, saletotalExclusiveVATAmount: 0, vatAmount: 0), receiptNumber: "receiptNumber")
    }
    
    
    private func createPosResponse() -> PosResponse {
        return PosResponse(customerName: ShopApiMock.customerName, referenceCode: ShopApiMock.referenceCode, gateCode: nil, carts: Array(carts.values))
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    public func activate(sessionId: Int64, completion: @escaping (ApiError?) -> Void) {
        if  sessionId == session.id {
//            sleep(1)
            completion(nil)
        } else {
//            sleep(1)
            completion(ApiError.badRequest(causeBy: nil))
        }
    }
    
	
	
	
	
	
	
	public func create(identificationNumber: String, createCart: Bool, completion: @escaping (Result<Session, ApiError>) -> Void) {
		if identificationNumber == ShopApiMock.validIdentificationNumber {
			if createCart {
				let cart = createNewCart()
				carts[cart.id] = cart
			}
			
			//            sleep(1)
			completion(.success(session))
		} else {
			//            sleep(1)
			completion(.failure(ApiError.badRequest(causeBy: nil)))
		}
	}
	
	public func addItem(sessionId: Int64, cartId: String, barcode: String, quantity: Double, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		if var cart = carts[cartId] {
			if let item = register[barcode] {
				cart.items.append(item)
				
				completion(.success(createPosResponse()))
			} else {
				completion(.failure(ApiError.badRequest(causeBy: nil)))
			}
		} else {
			completion(.failure(ApiError.badRequest(causeBy: nil)))
		}
	}
	
	public func updateItem(sessionId: Int64, cartId: String, cartItemId: String, barcode: String, quantity: Double, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		
	}
	
	public func deleteItem(sessionId: Int64, cartId: String, cartItemId: String, barcode: String, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		
	}
	
	public func addCart(sessionId: Int64, cartId: String, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		
	}
	
	public func getCarts(sessionId: Int64, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		completion(.success(createPosResponse()))
	}
	
	public func deleteCart(sessionId: Int64, cartId: String, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		
	}
	
	public func finalize(sessionId: Int64, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		
	}
	
	public func cancel(sessionId: Int64, completion: @escaping (Result<PosResponse, ApiError>) -> Void) {
		
	}
}
