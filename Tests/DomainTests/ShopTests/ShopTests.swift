import XCTest
@testable import Domain


class ShopTests: XCTestCase {
    
    var shop: Shop?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        shop = createShop()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        shop = nil
    }
    
    func testCreateAndActivateSession_success() {
        let semaphore = DispatchSemaphore(value: 0)
        
        shop!.start(identificationNumber: ShopApiMock.validIdentificationNumber, withDefaultCart: true) { (error) in
            XCTAssert(error == nil)
            semaphore.signal()
        }
        
        semaphore.wait()
    }
    
    func testcreateAndActivateSession_fail() {
        let semaphore = DispatchSemaphore(value: 0)

        shop!.start(identificationNumber: ShopApiMock.invalidIdentificationNumber, withDefaultCart: true) { (error) in
            XCTAssert(error != nil)
            semaphore.signal()
        }
        semaphore.wait()
    }
    
    func testAddItem_success() {
        let testObserver = TestObserver()
        let semaphore = DispatchSemaphore(value: 0)
        
        shop!.addObserver(testObserver)
        
        shop!.start(identificationNumber: ShopApiMock.validIdentificationNumber, withDefaultCart: true) { (error) in
            XCTAssert(error == nil)
            self.shop!.getCarts { (error) in
                self.shop!.addItem(barcode: ShopApiMock.barcode1, quantity: 1) { (error) in
                    XCTAssert(error == nil)
                    XCTAssert(testObserver.hasBeenNotified == true)
                    semaphore.signal()
                }
            }
        }
        semaphore.wait()
    }
    
    func testAddItem_fail() {
        let semaphore = DispatchSemaphore(value: 0)
        
        shop!.start(identificationNumber: ShopApiMock.validIdentificationNumber, withDefaultCart: true) { (error) in
            XCTAssert(error == nil)
            self.shop!.getCarts { (error) in
                let testObserver = TestObserver()
                self.shop!.addObserver(testObserver)
                self.shop!.addItem(barcode: "tjoflöjt", quantity: 1) { (error) in
                    XCTAssert(error != nil)
                    XCTAssert(testObserver.hasBeenNotified != true)
                    semaphore.signal()
                }
            }
        }
        semaphore.wait()
    }
    
    func createShop() -> Shop {
        return ShopImpl(api: ShopApiMock())
    }
}

class TestObserver {
	
	var id: String = UUID.init().uuidString
	var hasBeenNotified: Bool = false
	
	func notifyUpdate() {
		hasBeenNotified = true
	}
}

extension Shop {
	
	func addObserver(_ observer: TestObserver) {
		self.addObserver(with: observer.id) { _ in
			observer.notifyUpdate()
		}
	}
}


