//
//  Shop.swift
//  Domain
//
//  Created by Carl-Johan Dahlman on 2019-10-23.
//

import Foundation


public protocol Shop {
    var sessionId: Int64 {get}
    var carts: [Cart] {get}
    var activeCarts: [Cart] {get}
    var selectedCart: Cart? {get set}
    var hasControl: Bool {get}
    var gateCode: String? {get}
    
    // Start session
    func legacyStart(referenceCode: String, cartId: String?, completion: @escaping (StartShopError?) -> Void)
    func start(referenceCode: String, cartId: String?, completion: @escaping (StartShopError?) -> Void)
    
    // Recover session
    func recover(completion: @escaping (Result<Session.State, ShopError>) -> Void)
    func recover(fromReferenceCode: String, completion: @escaping (Result<Session.State, ShopError>) -> Void)
    
    // Manage selected cart
    func addItem(barcode: String, quantity: Double, completion: @escaping (ShopError?) -> Void)
    func updateItem(cartItemId: String, barcode: String, quantity: Double, completion: @escaping (ShopError?) -> Void)
    func moveItem(cartItemId: String, barcode: String, quantity: Double, completion: @escaping (ShopError?) -> Void)
    func deleteItem(cartItemId: String, barcode: String, completion: @escaping (ShopError?) -> Void)
    
    // Manage carts
    func addCart(cartId: String, completion: @escaping (ShopError?) -> Void)
    func getCarts(completion: @escaping (ShopError?) -> Void)
    func deleteCart(cartId: String, completion: @escaping (ShopError?) -> Void)
    
    // Manage session state
    func finalize(devicePayment: Bool,completion: @escaping (ShopError?) -> Void)
    func cancel(completion: @escaping (ShopError?) -> Void)
    func clearControl(completion: @escaping (ShopError?) -> Void)
        
    // Observers
    func addObserver(with id: String, callback: @escaping (Self) -> ())
    func removeObserver(with id: String)
    func notifyObservers()
}

