//
//  ShopImpl.swift
//  Domain
//
//  Created by Carl-Johan Dahlman on 2019-10-23.
//

import Foundation


public final class ShopImpl: Shop {
    
    var api: ShopApi
    
    public init(api: ShopApi) {
        self.api = api
    }
    
    public init(api: ShopApi, session: Session) {
        self.api = api
        self._session = session
    }
    
    private var _hasControl: Bool?
    public var hasControl: Bool {
        get {
            return _hasControl ?? false
        }
    }
    
    private var _gateCode: String?
    public var gateCode: String? {
        get {
            return _gateCode
        }
    }
    
    private var _session: Session?
    public var sessionId: Int64 {
        get {
            return _session?.id ?? 0
        }
    }
    
    public typealias Observer = (ShopImpl) -> ()
    var observers : [String: Observer] = [:]
    
    public var carts: [Cart] = []
    
    public var activeCarts: [Cart] {
        get {
            var actives: [Cart]?
            sync {
                actives = self.carts.filter { (cart) -> Bool in
                    cart.state == Cart.State.active
                }
            }
            return actives ?? []
        }
    }
    
    private var mSelectedCart: Cart?
    public var selectedCart: Cart? {
        get {
            sync {
                if self.carts.isEmpty {
                    self.mSelectedCart = nil
                }
				else {
                    if let mSelectedCart = self.mSelectedCart {
                        let activeCarts = self.carts.filter { (cart) -> Bool in
							switch cart.state {
							case .active, .completed: return true
							default: return false
							}
                        }
                        
                        if !activeCarts.contains(mSelectedCart) {
                            self.mSelectedCart = activeCarts.first
                        }
                        else {
                            self.mSelectedCart = activeCarts.first(where: { $0 == mSelectedCart })
                        }
                    }
                    else {
                        self.mSelectedCart = self.carts.first
                    }
                }
            }
            return mSelectedCart
        }
        
        set {
            sync {
                let activeCarts = self.carts.filter {
                    $0.state == Cart.State.active
                }
                
                if activeCarts.contains(where: { $0 == newValue }) {
                    self.mSelectedCart = newValue
                }
            }
        }
    }
    
    private func handleSelectedCartError(completion: @escaping (ShopError?) -> Void){
        completion(ShopError.noSelectedCart)
    }
    
    private func handleSessionError(completion: @escaping (ShopError?) -> Void) {
        completion(ShopError.noSession)
    }
    
    private func handleApiError(apiError: ApiError?, completion: @escaping (ShopError?) -> Void){
        completion(ShopError.apiError(causeBy: apiError))
    }
    
    private func handlePosResponse(response: PosResponse) {
        sync {
            self.carts = response.carts
            self._hasControl = response.hasControl
            self._gateCode = response.gateCode
            self.notifyObservers()
        }
    }
    
    public func legacyStart(referenceCode: String, cartId: String?, completion: @escaping (StartShopError?) -> Void) {
        async {
            var savedError: StartShopError?
            
            let createSemaphore = DispatchSemaphore(value: 0)
            self.api.create(identificationNumber: referenceCode, createCart: cartId == nil) { (response) in
                switch response {
                case .success(let session):
                    self._session = session
                case .failure(let error):
                    savedError = StartShopError.createSessionError(causeBy: error)
                }
                createSemaphore.signal()
            }
            createSemaphore.wait()
            
            if savedError == nil {
                let activateSemaphore = DispatchSemaphore(value: 0)
                self.api.activate(sessionId: self._session!.id) { (error) in
                    if error != nil {
                        savedError = StartShopError.activateSessionError(causeBy: error)
                    }
                    activateSemaphore.signal()
                }
                activateSemaphore.wait()
            }
            
            if let cartId = cartId {
                let createCartSemaphore = DispatchSemaphore(value: 0)
                self.api.addCart(sessionId: self._session!.id, cartId: cartId) { result in
                    switch result {
                    case .success(let pos): self.handlePosResponse(response: pos)
                    case .failure(let error): savedError = StartShopError.createCartError(causeBy: error)
                    }
                    createCartSemaphore.signal()
                }
                createCartSemaphore.wait()
            }
            
            if savedError == nil {
                let cartsSemaphore = DispatchSemaphore(value: 0)
                self.api.getCarts(sessionId: self.sessionId) { (result) in
                    switch result {
                    case .success(let pos): self.handlePosResponse(response: pos)
                    case .failure(let error): savedError = StartShopError.getCartsError(causeBy: error)
                    }
                    cartsSemaphore.signal()
                }
                cartsSemaphore.wait()
            }
            
            DispatchQueue.main.async {
                completion(savedError)
            }
        }
    }
    
    public func start(referenceCode: String, cartId: String?, completion: @escaping (StartShopError?) -> Void) {
        self.api.createMobileSession(identificationNumber: referenceCode, createCart: true, receiptLabel: cartId) { result in
            switch result {
            case .success(let session):
                self.api.getCarts(sessionId: session.id) { result in
                    switch result {
                    case .success(let posResponse):
                        self._session = session
                        self.handlePosResponse(response: posResponse)
                        completion(nil)
                    case .failure(let error):
                        completion(.getCartsError(causeBy: error))
                    }
                }
            case .failure(let error):
                completion(.createSessionError(causeBy: error))
            }
        }
    }
    
    public func recover(completion: @escaping (Result<Session.State, ShopError>) -> Void) {
        guard let sessionId = _session?.id else {
            completion(.failure(.noSession))
            return
        }
        
        api.getSession(sessionId: sessionId) { result in
            switch result {
            case .success(let session):
                self.handleRecoverSession(session) { error in
                    if let error = error {
                        completion(.failure(error))
                    }
                    else {
                        completion(.success(session.state))
                    }
                }
            case .failure(let error):
                completion(.failure(ShopError.apiError(causeBy: error)))
            }
        }
    }
    
    public func recover(fromReferenceCode referenceCode: String, completion: @escaping (Result<Session.State, ShopError>) -> Void) {
        api.getSession(referenceCode: referenceCode) { result in
            switch result {
            case .success(let session):
                self.handleRecoverSession(session) { error in
                    if let error = error {
                        completion(.failure(error))
                    }
                    else {
                        completion(.success(session.state))
                    }
                }
            case .failure(let error):
                completion(.failure(ShopError.apiError(causeBy: error)))
            }
        }
    }
    
    private func handleRecoverSession(_ session: Session, completion: @escaping (ShopError?) -> Void) {
        switch session.state {
        case .pendingStart:
            api.activate(sessionId: session.id) { error in
                if let error = error {
                    completion(.apiError(causeBy: error))
                }
                else {
                    self._session = session
                    self.fetchCarts(completion: completion)
                }
            }
        case .active:
            self._session = session
            self.fetchCarts(completion: completion)
        case .pendingPayment, .control:
            self._session = session
            self.finalize(devicePayment: true, completion: completion)
        default:
            // If finished, rescueMode, canceled, timedOut, or pendingReceipt we can't recover session
            completion(.invalidState(state: session.state))
        }
    }
    
    private func fetchCarts(completion: @escaping (ShopError?) -> Void) {
        self.api.getCarts(sessionId: self.sessionId) { (result) in
            switch result {
            case .success(let pos):
                self.handlePosResponse(response: pos)
                completion(nil)
            case .failure(let error):
                completion(.apiError(causeBy: error))
            }
        }
    }
    
    private func actionResponseHandler(response: Result<PosResponse, ApiError>) -> ShopError? {
        switch response {
        case .success(let pos):
            self.handlePosResponse(response: pos)
            return nil
        case .failure(let error):
            return ShopError.apiError(causeBy: error)
        }
    }
    
    
    public func addItem(barcode: String, quantity: Double, completion: @escaping (ShopError?) -> Void) {
        guard selectedCart != nil else {
            handleSelectedCartError(completion: completion)
            return
        }
        
		self.api.addItem(sessionId: self.sessionId, cartId: self.selectedCart!.id, barcode: barcode, quantity: quantity) { (response) in
			completion(self.actionResponseHandler(response: response))
		}
    }
    
    public func updateItem(cartItemId: String, barcode: String, quantity: Double, completion: @escaping (ShopError?) -> Void) {
        guard selectedCart != nil else {
            handleSelectedCartError(completion: completion)
            return
        }
        
		self.api.updateItem(sessionId: self.sessionId, cartId: self.selectedCart!.id, cartItemId: cartItemId, barcode: barcode, quantity: quantity) { (response) in
			completion(self.actionResponseHandler(response: response))
		}
    }
    
    public func moveItem(cartItemId: String, barcode: String, quantity: Double, completion: @escaping (ShopError?) -> Void) {
        guard selectedCart != nil else {
            handleSelectedCartError(completion: completion)
            return
        }
        
        // TODO
    }
    
    public func deleteItem(cartItemId: String, barcode: String, completion: @escaping (ShopError?) -> Void) {
        guard selectedCart != nil else {
            handleSelectedCartError(completion: completion)
            return
        }
        
		self.api.deleteItem(sessionId: self.sessionId, cartId: self.selectedCart!.id, cartItemId: cartItemId, barcode: barcode) { (response) in
			completion(self.actionResponseHandler(response: response))
		}
    }
    
    public func addCart(cartId: String, completion: @escaping (ShopError?) -> Void) {
		self.api.addCart(sessionId: self.sessionId, cartId: cartId) { (response) in
			completion(self.actionResponseHandler(response: response))
		}
    }
    
    public func getCarts(completion: @escaping (ShopError?) -> Void) {
		self.api.getCarts(sessionId: self.sessionId) { (response) in
			completion(self.actionResponseHandler(response: response))
        }
    }
    
    public func deleteCart(cartId: String, completion: @escaping (ShopError?) -> Void) {
        guard selectedCart != nil else {
            handleSelectedCartError(completion: completion)
            return
        }
        
		self.api.deleteCart(sessionId: self.sessionId, cartId: cartId ) { (response) in
			completion(self.actionResponseHandler(response: response))
		}
    }
    
    public func finalize(devicePayment: Bool,completion: @escaping (ShopError?) -> Void) {
        guard _session != nil else {
            handleSessionError(completion: completion)
            return
        }
        
		self.api.finalize(sessionId: self.sessionId, devicePayment: devicePayment) { (response) in
			completion(self.actionResponseHandler(response: response))
		}
    }
    
    public func cancel(completion: @escaping (ShopError?) -> Void) {
		self.api.cancel(sessionId: self.sessionId) { (response) in
			completion(self.actionResponseHandler(response: response))
		}
    }
    
    public func clearControl(completion: @escaping (ShopError?) -> Void) {
        guard selectedCart != nil else {
            handleSelectedCartError(completion: completion)
            return
        }
        
		self.api.clearControl(sessionId: self.sessionId) { error in
			completion(error.map { ShopError.apiError(causeBy: $0) })
		}
    }
    
    public func addObserver(with id: String, callback: @escaping Observer) {
        observers[id] = callback
    }
    
    public func removeObserver(with id: String) {
        observers[id] = nil
    }
    
    public func notifyObservers() {
        observers.values.forEach { $0(self) }
    }
}
