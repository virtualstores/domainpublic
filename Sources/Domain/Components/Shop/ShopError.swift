//
//  ShopError.swift
//  Domain
//
//  Created by Carl-Johan Dahlman on 2019-10-23.
//

import Foundation


public enum ShopError: Error {
	case noSelectedCart
	case noSession
    case invalidState(state: Session.State)
	case apiError(causeBy: Error?)
}

public enum StartShopError: Error {
    case createSessionError(causeBy: ApiError?)
    case activateSessionError(causeBy: ApiError?)
    case createCartError(causeBy: ApiError?)
    case getCartsError(causeBy: ApiError?)
}
