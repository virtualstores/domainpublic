//
//  ControlShop.swift
//  Domain
//
//  Created by Théodore Roos on 2020-08-17.
//

import Foundation

public protocol ControlShop {
    var sessionId: Int64 { get }
    var controlFlag: Int { get }
    var carts: [Cart] { get }
    var selectedCart: Cart? { get set }
    
    // Start session
    func start(forSession sessionId: Int64, employeeId: String, completion: @escaping (Error?) -> Void)
    
    // Manage selected cart
    func addItem(barcode: String, quantity: Double, completion: @escaping (Error?) -> Void)
    func updateItem(cartItemId: String, barcode: String, quantity: Double, completion: @escaping (Error?) -> Void)
    func deleteItem(cartItemId: String, barcode: String, completion: @escaping (Error?) -> Void)
    
    // Manage session state
    func report(forceSuccess: Bool, completion: @escaping (Error?) -> Void)
    
    // Observers
    func addObserver(with id: String, callback: @escaping (Self) -> Void)
    func removeObserver(with id: String)
    func notifyObservers()
}
