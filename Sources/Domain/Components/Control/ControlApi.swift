//
//  ControlApi.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-08-05.
//

import Foundation

public protocol ControlApi {
    func getCarts(forSession sessionId: Int64, employeeId: String, completion: @escaping (Result<ControlPosResponse, Error>) -> Void)
    
    func reportCart(forSession sessionId: Int64, forceSuccess: Bool, completion: @escaping (Error?) -> Void)
    
    func addItem(forSession sessionId: Int64, cartId: String, barcode: String, quantity: Double, completion: @escaping (Result<ControlPosResponse, Error>) -> Void)
    
    func updateItem(forSession sessionId: Int64, cartId: String, cartItemId: String, barcode: String, quantity: Double, completion: @escaping (Result<ControlPosResponse, Error>) -> Void)
    
    func deleteItem(forSession sessionId: Int64, cartId: String, cartItemId: String, barcode: String, completion: @escaping (Result<ControlPosResponse, Error>) -> Void)
}
