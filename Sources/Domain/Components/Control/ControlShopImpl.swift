//
//  ControlShopImpl.swift
//  Domain
//
//  Created by Théodore Roos on 2020-08-17.
//

import Foundation

public final class ControlShopImpl: ControlShop {
    
    var api: ControlApi
    
    public init(api: ControlApi) {
        self.api = api
    }
    
    public init(api: ControlApi, session: Session) {
        self.api = api
        self._session = session
    }
    
    private var _session: Session?
    public var sessionId: Int64 {
        get {
            return _session?.id ?? 0
        }
    }
    
    private var _controlFlag: Int?
    public var controlFlag: Int {
        get {
            return _controlFlag ?? 0
        }
    }
    
    public typealias Observer = (ControlShopImpl) -> Void
    var observers: [String: Observer] = [:]
    
    private var _carts: [Cart]?
    public var carts: [Cart] {
        get {
            return _carts ?? []
        }
    }
    
    private var mSelectedCart: Cart?
    public var selectedCart: Cart? {
        get {
            sync {
                if self.carts.isEmpty {
                    self.mSelectedCart = nil
                }
                else {
                    mSelectedCart = carts.first
                }
            }
            
            return mSelectedCart
        }
        
        set {
            sync {
                mSelectedCart = newValue
            }
        }
    }
    
    private func handlePosResponse(response: ControlPosResponse) {
        sync {
            self._controlFlag = response.controlFlag
            self._carts = response.posResponse.carts
            self.notifyObservers()
        }
    }
    
    public func start(forSession sessionId: Int64, employeeId: String, completion: @escaping (Error?) -> Void) {
        self._session = Session(id: sessionId, state: .control)
        
        self.api.getCarts(forSession: self.sessionId, employeeId: employeeId) { (result) in
            switch result {
            case .success(let response):
                self.handlePosResponse(response: response)
                completion(nil)
            case .failure(let error):
                print(error)
                completion(error)
            }
        }
    }
    
    public func addItem(barcode: String, quantity: Double, completion: @escaping (Error?) -> Void) {
        guard let cartId = selectedCart?.id else {
            return
        }
        
        api.addItem(forSession: self.sessionId, cartId: cartId, barcode: barcode, quantity: quantity) { (result) in
            switch result {
            case .success(let response):
                self.handlePosResponse(response: response)
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    public func updateItem(cartItemId: String, barcode: String, quantity: Double, completion: @escaping (Error?) -> Void) {
        guard let cartId = selectedCart?.id else {
            return
        }
        
        api.updateItem(forSession: sessionId, cartId: cartId, cartItemId: cartItemId, barcode: barcode, quantity: quantity) { (result) in
            switch result {
            case .success(let response):
                self.handlePosResponse(response: response)
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    public func deleteItem(cartItemId: String, barcode: String, completion: @escaping (Error?) -> Void) {
        guard let cart = selectedCart, let itemId = cart.items.first(where: { $0.id == cartItemId })?.id else {
            return
        }
        api.deleteItem(forSession: sessionId, cartId: cart.id, cartItemId: itemId, barcode: barcode) { (result) in
            switch result {
            case .success(let response):
                self.handlePosResponse(response: response)
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    public func report(forceSuccess: Bool, completion: @escaping (Error?) -> Void) {
        api.reportCart(forSession: self.sessionId, forceSuccess: forceSuccess) { (error) in
            switch error {
            case .none:
                completion(nil)
            case .some(let error):
                completion(error)
            }
        }
    }
    
    public func addObserver(with id: String, callback: @escaping Observer) {
        observers[id] = callback
    }
    
    public func removeObserver(with id: String) {
        observers[id] = nil
    }
    
    public func notifyObservers() {
        observers.values.forEach( { $0(self) } )
    }
}
