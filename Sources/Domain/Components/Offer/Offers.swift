//
//  Offer.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-20.
//

import Foundation

public protocol Offers {
    func getOffersBy(customerId: String, completion: @escaping (Result<[Offer], ApiError>) -> Void)
}
