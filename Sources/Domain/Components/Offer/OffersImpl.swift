//
//  OffersImpl.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-20.
//

import Foundation

public class OffersImpl: Offers {


    let offerApi: OfferApi

    public init (offerApi: OfferApi){
    self.offerApi = offerApi
}

    public func getOffersBy(customerId: String, completion: @escaping (Result<[Offer], ApiError>) -> Void) {
        offerApi.getOffersBy(customerId: customerId, completion: { (response) in
            switch response {
            case .success(let offers):
                completion(.success(offers))
                
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
}
