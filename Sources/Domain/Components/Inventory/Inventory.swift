//
//  ItemsProtocol.swift
//  domain
//
//  Created by Carl-Johan Dahlman on 2019-10-14.
//

import Foundation


public protocol Inventory {
	
    func getItemBy(id: String, completion: @escaping (Item?) -> Void)
    
    func search(withExtension: Bool, searchValue: String, completion: @escaping (SearchResult) -> Void) -> Cancellable
    
    func search(withExtension: Bool, searchValue: String, itemGroupId: String, completion: @escaping (SearchResult) -> Void) -> Cancellable
    
    func getRecommendedItems(for memberId: String, completion: @escaping (Result<[Item], Error>) -> Void)
    
    func getItemGroups(limit: Int32, offset: Int32, completion: @escaping (Result<[ItemGroup], Error>) -> Void)
    
    func getItemGroupDetail(id: String, completion: @escaping (Result<ItemGroupDetail, Error>) -> Void)
    
    func getItemDetail(id: String, completion: @escaping (Result<ItemDetail, Error>) -> Void)
    
}

