import Foundation

public class InventoryImpl: Inventory {
    
    public init (itemApi: ItemApi, searchApi: SearchApi){
        self.itemApi = itemApi
        self.searchApi = searchApi
    }
    
    let itemApi: ItemApi
    let searchApi: SearchApi
    
    public func getItemBy(id: String, completion: @escaping (Item?) -> Void) {
        itemApi.getItemBy(id: id) { (response) in
			switch response {
			case .success(let item):
				completion(item)
				
			case .failure(let error):
				//TODO: Maybe handle this error
				print(error.localizedDescription)
				completion(nil)
			}
        }
    }
    
    public func search(withExtension: Bool, searchValue: String, completion: @escaping (SearchResult) -> Void) -> Cancellable {
        return search(withExtension: withExtension, searchValue: searchValue, itemGroupId: "", completion: completion)
    }
    
    public func search(withExtension: Bool, searchValue: String, itemGroupId: String, completion: @escaping (SearchResult) -> Void) -> Cancellable {
        return searchApi.findBy(withExtension: withExtension, searchvalue: searchValue, onlyWithPosition: true, itemGroupId: itemGroupId, limit: 60) { (response) in
			switch response {
			case .success(let result):
				completion(result)
				
			case .failure(let error):
				//TODO: Maybe handle this error
				print(error.localizedDescription)
				completion(SearchResult(itemGroups: [], items: []))
			}
		}
    }
    
    public func getRecommendedItems(for memberId: String, completion: @escaping (Result<[Item], Error>) -> Void) {
        return itemApi.getRecommendedItems(for: memberId, completion: completion)
    }
    
    public func getItemGroups(limit: Int32, offset: Int32, completion: @escaping (Result<[ItemGroup], Error>) -> Void) {
        itemApi.getItemGroup(limit: limit, offset: offset, completion: completion)
    }
    
    public func getItemGroupDetail(id: String, completion: @escaping (Result<ItemGroupDetail, Error>) -> Void) {
        itemApi.getItemGroupDetail(id: id, completion: completion)
    }
    
    public func getItemDetail(id: String, completion: @escaping (Result<ItemDetail, Error>) -> Void) {
        return itemApi.getItemDetail(id: id, completion: completion)
    }
}
