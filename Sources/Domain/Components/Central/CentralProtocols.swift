//
//  Central.swift
//  domain
//
//  Created by Carl-Johan Dahlman on 2019-10-10.
//

import Foundation

public protocol CentralApi {
	
    func getStores(completion: @escaping (Result<[Store], Error>) -> Void)
    
    func verifyCustomer(username: String, password: String, clientId: Int64, completion: @escaping (Result<User, Error>) -> Void)

    func verifyEmployee(clientId: Int64, employeeIdentifier: String, completion: @escaping (Result<Employee, Error>) -> Void)
    
    func getNavigationSpaces(completion: @escaping (Result<[NavigationSpaceInfo], Error>) -> Void)
    
    func getNavigationSpace(for id: Int64, completion: @escaping (Result<NavigationSpace, Error>) -> Void)
    
    func getItemPosition(storeId: Int64, itemId: String, completion: @escaping (Result<[BarcodePosition], Error>) -> Void)
    
    func postSessionActivate(storeId: Int64, sessionId: String, device: DeviceInformation, completion: @escaping (Error?) -> Void)
    
    func postOrders(storeId: Int64, orderIds: [String], device: DeviceInformation, completion: @escaping (Error?) -> Void)
}

public extension CentralApi {
    func getNavigationSpaces(completion: @escaping (Result<[NavigationSpaceInfo], Error>) -> Void) {
        getStores { result in
            switch result {
            case .success(let stores):
                completion(.success(stores.map { NavigationSpaceInfo(id: $0.id, name: $0.name) }))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
