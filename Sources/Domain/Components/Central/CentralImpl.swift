//
//  CentralImpl.swift
//  domain
//
//  Created by Carl-Johan Dahlman on 2019-10-10.
//

import Foundation


public class CentralImpl: CentralApi {
    
    let api: CentralApi
    
    init(api: CentralApi) {
        self.api = api
    }
    
    public func getStores(completion: @escaping (Result<[Store], Error>) -> Void) {
        return api.getStores(completion: completion)
    }
    
    public func getStores(clientId: Int64, completion: @escaping ([Store]) -> Void) {
        api.getStores() { (response) in
            completion((try? response.get()) ?? [])
        }
    }
    
    public func verifyCustomer(username: String, password: String, clientId: Int64, completion: @escaping (Result<User, Error>) -> Void) {
        return api.verifyCustomer(username: username, password: password, clientId: clientId, completion: completion)
    }
    
    public func verifyEmployee(clientId: Int64, employeeIdentifier: String, completion: @escaping (Result<Employee, Error>) -> Void) {
        return api.verifyEmployee(clientId: clientId, employeeIdentifier: employeeIdentifier, completion: completion)
    }
    
    public func getNavigationSpaces(completion: @escaping (Result<[NavigationSpaceInfo], Error>) -> Void) {
        return api.getNavigationSpaces(completion: completion)
    }
    
    public func getNavigationSpace(for id: Int64, completion: @escaping (Result<NavigationSpace, Error>) -> Void) {
        return api.getNavigationSpace(for: id, completion: completion)
    }
    
    public func getItemPosition(storeId: Int64, itemId: String, completion: @escaping (Result<[BarcodePosition], Error>) -> Void) {
        return api.getItemPosition(storeId: storeId, itemId: itemId, completion: completion)
    }
    
    public func postSessionActivate(storeId: Int64, sessionId: String, device: DeviceInformation, completion: @escaping (Error?) -> Void) {
        return api.postSessionActivate(storeId: storeId, sessionId: sessionId, device: device, completion: completion)
    }
    
    public func postOrders(storeId: Int64, orderIds: [String], device: DeviceInformation, completion: @escaping (Error?) -> Void) {
        return api.postOrders(storeId: storeId, orderIds: orderIds, device: device, completion: completion)
    }
}
