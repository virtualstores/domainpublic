//
//  NavigationImpl.swift
//  domain
//
//  Created by Carl-Johan Dahlman on 2019-10-16.
//

import Foundation


public class NavigationImpl {
    let storeId: Int64
    let shelfApi: ShelfApi
    let mapApi: MapApi
    let rtls: Store.RTLS
    
    var shelfGroups: [ShelfGroup] = []
    var shelfTierItemPositions: [Int64: ItemPosition] = [:]
    var navigationSpaces: [NavigationSpace] = []
    var map: Map?
    
    public init(storeId: Int64, shelfApi: ShelfApi,
                mapApi: MapApi, rtls: Store.RTLS){
        self.storeId = storeId
        self.shelfApi = shelfApi
        self.mapApi = mapApi
        self.rtls = rtls
    }
}

extension NavigationImpl: Navigation {
    public func initate(clientId: Int64, completion: @escaping (Error?) -> Void) {
        async {
            self.shelfTierItemPositions = [:]
            
            let shelfGroupSemaphore = DispatchSemaphore(value: 0)
            self.shelfApi.getShelfGroups(storeID: self.storeId) { (response) in
                switch response {
                case .success(let groups):
                    self.shelfGroups = groups
                    for group in groups {
                        for shelf in group.shelves {
                            for tier in shelf.shelfTiers {
                                self.shelfTierItemPositions[tier.id] = shelf.itemPosition
                            }
                        }
                    }
                case .failure(let error):
                    async(queue: .main) {
                        completion(error)
                    }
                }
                
                shelfGroupSemaphore.signal()
            }
            
            let mapSemaphore = DispatchSemaphore(value: 0)
            self.mapApi.getBy(clientId: clientId, storeId: self.storeId) { (response) in
                switch response {
                case .success(let map):
                    self.map = map
                    mapSemaphore.signal()
                case .failure(let error):
                    async(queue: .main) {
                        completion(error)
                    }
                }
            }
            mapSemaphore.wait()
            
            shelfGroupSemaphore.wait()
            
            async(queue: .main) {
                completion(nil)
            }
        }
    }
    
    public func getItemPosition(shelfTierId: Int64) -> ItemPosition? {
        return shelfTierItemPositions[shelfTierId]
    }
    
    public func getShelfGroups() -> [ShelfGroup] {
        return shelfGroups
    }
    
    public func getNavigationSpaces() -> [NavigationSpace] {
        return navigationSpaces
    }
    
    public func getMap() -> Map? {
        return map
    }
    
    public func getRtlsOption() -> Store.RTLS? {
        return rtls
    }
}
