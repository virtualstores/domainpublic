//
//  Navigation.swift
//  domain
//
//  Created by Carl-Johan Dahlman on 2019-10-16.
//

import CoreGraphics
import CoreLocation


public protocol Navigation {
	
    func initate(clientId: Int64, completion: @escaping (Error?) -> Void)

    func getItemPosition(shelfTierId: Int64) -> ItemPosition?
    
    func getShelfGroups() -> [ShelfGroup]
    
    func getRtlsOption() -> Store.RTLS?
}
