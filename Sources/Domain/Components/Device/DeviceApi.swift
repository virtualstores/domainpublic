//
//  DeviceApi.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-04-15.
//

import Foundation

public protocol DeviceApi {
    func minimumVersion(completion: @escaping (Result<String, Error>) -> Void)
}
