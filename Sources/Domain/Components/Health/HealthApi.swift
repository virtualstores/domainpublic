//
//  HealthApi.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-06-02.
//

import Foundation

public protocol HealthApi {
    func health(completion: @escaping (Result<Health, Error>) -> Void)
}
