//
//  InformationManager.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-06-25.
//

import Foundation
import CoreGraphics
import UIKit

public protocol InformationManager {
    var ready: Bool { get }
    var allMessages: [Message] { get }
    var triggeredMessages: Set<Message> { get set }
    
    func loadMessages(completion: @escaping (Error?) -> Void)
    // Points is in degrees
    func messages(for point: CGPoint) -> [Message]
    func allMessages(for point: CGPoint) -> [Message]
}

public extension InformationManager {
    fileprivate func zoneContains(point: CGPoint, zone: [CGPoint]) -> Bool {
        let path = UIBezierPath()
        path.move(to: zone[0])
        zone[1...].forEach {
            path.addLine(to: $0)
        }
        return path.contains(point)
    }
    
    func messages(for point: CGPoint) -> [Message] {
        allMessages.filter {
            !triggeredMessages.contains($0) &&
            $0.duration.contains(.init()) &&
            ( $0.exposureType == .wholeMap || $0.zones.contains { zoneContains(point: point, zone: $0) } )
        }
    }
    
    func allMessages(for point: CGPoint) -> [Message] {
        allMessages.filter {
            $0.duration.contains(.init()) &&
            ( $0.exposureType == .wholeMap || $0.zones.contains { zoneContains(point: point, zone: $0) } )
        }
    }
}
