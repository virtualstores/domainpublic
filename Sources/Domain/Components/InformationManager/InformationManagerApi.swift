//
//  InformationManagerApi.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-06-24.
//

import Foundation

public protocol InformationManagerApi {
    func getMessages(completion: @escaping (Result<[Message], Error>) -> Void)
}
