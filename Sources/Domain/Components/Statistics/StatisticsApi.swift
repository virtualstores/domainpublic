//
//  StatisticsApi.swift
//  Api
//
//  Created by Jesper Lundqvist on 2020-02-25.
//

import Foundation

public struct VisitPosition: Codable {
    public var _id: Int64

    public var visitId: Int64

    public var x: Int64

    public var y: Int64

    public var health: Double

    public var direction: Double

    public var timestamp: Int64
    public init(_id: Int64, visitId: Int64, x: Int64, y: Int64, health: Double, direction: Double, timestamp: Int64) {
        self._id = _id
        self.visitId = visitId
        self.x = x
        self.y = y
        self.health = health
        self.direction = direction
        self.timestamp = timestamp
    }
    
    public enum CodingKeys: String, CodingKey {
        case _id = "id"
        case visitId
        case x
        case y
        case health
        case direction
        case timestamp
    }
}

public struct Visit: Codable {
    public var sessionId: Int64

    public var customerHeight: Int64

    public var start: Int64

    public var stop: Int64

    public var positionType: String

    public var operatingSystem: String

    public var osVersion: String

    public var appVersion: String

    public var deviceModel: String

    public var positions: [VisitPosition]

    public init(sessionId: Int64, customerHeight: Int64, start: Int64, stop: Int64, positionType: String, operatingSystem: String, osVersion: String, appVersion: String, deviceModel: String, positions: [VisitPosition]) {
        self.sessionId = sessionId
        self.customerHeight = customerHeight
        self.start = start
        self.stop = stop
        self.positionType = positionType
        self.operatingSystem = operatingSystem
        self.osVersion = osVersion
        self.appVersion = appVersion
        self.deviceModel = deviceModel
        self.positions = positions
    }
}


public protocol StatisticsApi {
    func uploadVisit(_ visit: Visit, completion: @escaping (Error?) -> Void)
}
