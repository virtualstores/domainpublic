import Foundation
public class PaymentsImpl: Payments {
    
    let klarnaPaymentsApi: KlarnaPaymentsApi
    let payExApi: PayExCardPaymentsApi
    
    public init(klarnaPaymentsApi: KlarnaPaymentsApi, payExApi: PayExCardPaymentsApi) {
        self.klarnaPaymentsApi = klarnaPaymentsApi
        self.payExApi = payExApi
    }
    
    public var klarnaPayments: KlarnaPayments {
        get { return KlarnaPaymentsImpl(api: klarnaPaymentsApi) }
    }
    
    public var payExCardPayments: PayExCardPayment {
        get {
            return PayExCardPaymentImpl(api: payExApi)
        }
    }
}
