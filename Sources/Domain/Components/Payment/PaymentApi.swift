//
//  PaymentApi.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-02-27.
//

import Foundation

public protocol PaymentApi {
    func getPaymentInformation(sessionId: Int64, cartId: String, completion: @escaping (Result<PaymentInformation, Error>) -> Void)
}
