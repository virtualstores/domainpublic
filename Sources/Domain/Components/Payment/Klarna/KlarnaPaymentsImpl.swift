import Foundation

public class KlarnaPaymentsImpl: KlarnaPayments {
    
    let api: KlarnaPaymentsApi
    
    public init (api: KlarnaPaymentsApi) {
        self.api = api
    }
    
    public func start(sessionId: Int64, cartId: String, completion: @escaping (Result<KlarnaPaymentSession, Error>) -> Void) {
        api.start(sessionId: sessionId, cartId: cartId) { result in
            
        }
    }
    
    public func complete(sessionId: Int64, cartId: String, authToken: String) {
        api.complete(sessionId: sessionId, cartId: cartId, authToken: authToken) { result in
            
        }
    }
    
    public func cancel(sessionId: Int64, cartId: String) {
        api.cancel(sessionId: sessionId, cartId: cartId) { result in
            
        }
    }
}
