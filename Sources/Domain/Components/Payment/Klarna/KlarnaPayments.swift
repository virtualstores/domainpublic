//
//  KlarnaPayments.swift
//  Domain
//
//  Created by Carl-Johan Dahlman on 2019-12-02.
//

import Foundation

public protocol KlarnaPayments {
    func start(sessionId: Int64, cartId: String, completion: @escaping (Result<KlarnaPaymentSession, Error>) -> Void)
    func complete(sessionId: Int64, cartId: String, authToken: String)
    func cancel(sessionId: Int64, cartId: String)
}
