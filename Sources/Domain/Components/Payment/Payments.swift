import Foundation
public protocol Payments {
    var klarnaPayments: KlarnaPayments { get }
    var payExCardPayments: PayExCardPayment { get }
}
