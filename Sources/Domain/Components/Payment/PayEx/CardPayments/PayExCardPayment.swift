import Foundation
public protocol PayExCardPayment {
    func start(sessionId: Int64, cartId: String, generateToken: Bool?, paymentToken: String?, completion: @escaping (Result<PayExStartResponse, Error>) -> Void)
    func complete(sessionId: Int64, cartId: String)
    func cancel(sessionId: Int64, cartId: String)
}
