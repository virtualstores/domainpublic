import Foundation

public class PayExCardPaymentImpl: PayExCardPayment {
    
    let api: PayExCardPaymentsApi
    
    public init (api: PayExCardPaymentsApi) {
        self.api = api
    }
    
    public func start(sessionId: Int64, cartId: String, generateToken: Bool?, paymentToken: String?, completion: @escaping (Result<PayExStartResponse, Error>) -> Void) {
        api.start(sessionId: sessionId, cartId: cartId, generateToken: generateToken, paymentToken: paymentToken, completion: completion)
    }
    
    public func complete(sessionId: Int64, cartId: String) {
        api.complete(sessionId: sessionId, cartId: cartId) { result in
            
        }
    }
    
    public func cancel(sessionId: Int64, cartId: String) {
        api.cancel(sessionId: sessionId, cartId: cartId) { result in
        
        }
    }
}
