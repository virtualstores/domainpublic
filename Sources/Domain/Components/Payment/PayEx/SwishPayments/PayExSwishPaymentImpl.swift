import Foundation

public class PayExSwishPaymentImpl: PayExSwishPayment {
    
    let api: PayExSwishPaymentsApi
    
    public init (api: PayExSwishPaymentsApi) {
        self.api = api
    }
    
    public func start(sessionId: Int64, cartId: String, completion: @escaping (Result<PayExSwishStartResponse, Error>) -> Void) {
        api.start(sessionId: sessionId, cartId: cartId, completion: completion)
    }
    
    public func verify(sessionId: Int64, cartId: String, completion: @escaping (Error?) -> Void) {
        api.verify(sessionId: sessionId, cartId: cartId, completion: { result in
            switch result {
            case .none:
                completion(nil)
            case .some(let error):
                completion(error)
            }
        })
    }
    public func callback(sessionId: Int64, cartId: String, paymentId: String, completion: @escaping (Error?) -> Void) {
        api.callback(sessionId: sessionId, cartId: cartId, paymentId: paymentId, completion: { result in
            switch result {
            case .none:
                completion(nil)
            case .some(let error):
                completion(error)
            }
        })
    }
}
