import Foundation

public protocol PayExSwishPayment {
    func start(sessionId: Int64, cartId: String, completion: @escaping (Result<PayExSwishStartResponse, Error>) -> Void)
    func verify(sessionId: Int64, cartId: String, completion: @escaping (Error?) -> Void)
    func callback(sessionId: Int64, cartId: String, paymentId: String, completion: @escaping (Error?) -> Void)
}
