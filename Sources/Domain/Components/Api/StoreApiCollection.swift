import Foundation


public protocol StoreApiCollection {
    
    var itemApi: ItemApi { get }
    var searchApi: SearchApi { get }
    var shelfApi: ShelfApi { get }
    var mapApi: MapApi { get }
    var shopApi: ShopApi { get }
    var payExCardPaymentsApi: PayExCardPaymentsApi { get }
    var payExSwishPaymentsApi: PayExSwishPaymentsApi { get }
    var klarnaPaymentsApi: KlarnaPaymentsApi { get }
    var offerApi: OfferApi { get }
    var shoppingListApi: ShoppingListApi { get }
    var userApi: UserApi { get }
    var recipeApi: RecipeApi { get }
    var statisticsApi: StatisticsApi { get }
    var paymentApi: PaymentApi { get }
    var deviceApi: DeviceApi { get }
    var healthApi: HealthApi { get }
    var informationManagerApi: InformationManagerApi { get }
    var controlApi: ControlApi { get }
}
