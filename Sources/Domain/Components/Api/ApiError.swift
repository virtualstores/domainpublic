//
//  ApiError.swift
//  Domain
//
//  Created by Carl-Johan Dahlman on 2019-10-23.
//

import Foundation


public enum ApiError: Error {
	
    case badRequest(response: HTTPURLResponse, data: Data?)
    case serverError(response: HTTPURLResponse, data: Data?)
    case timeout(causeBy: Error?)
    case serialize(causeBy: Error?)
    case unknown(causeBy: Error?)
}
