//
//  Api.swift
//  domain
//
//  Created by Carl-Johan Dahlman on 2019-10-14.
//

import Foundation


public protocol Api {
    
    var central: CentralApiCollection { get }
    var store: StoreApiCollection? { get }
    
	@discardableResult
    func initStoreApi(storeConnection: ServerConnection) -> StoreApiCollection
}

