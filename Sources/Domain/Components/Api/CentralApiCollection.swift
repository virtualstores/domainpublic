import Foundation


public protocol CentralApiCollection {
	
    var centralApi: CentralApi { get }
}
