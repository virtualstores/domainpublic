//
//  UserImpl.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-23.
//

import Foundation

public class UserImpl: GenericUser {
    public init (userApi: UserApi){
        self.userApi = userApi
    }
    
    let userApi: UserApi
    
    public func verifyUser(username: String, password: String, completion: @escaping (Result<LocalUser, Error>) -> Void) {
        userApi.verifyUser(username: username, password: password) { (response) in
            switch response {
            case .success(let user):
                completion(.success(user))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    
}
