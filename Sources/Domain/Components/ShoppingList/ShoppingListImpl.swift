//
//  ShoppingListImpl.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-23.
//

import Foundation

public class ShoppingListImpl: ShoppingLists {
    public init (shoppingListApi: ShoppingListApi, user: User){
        self.shoppingListApi = shoppingListApi
        self.user = user
    }
    
    let shoppingListApi: ShoppingListApi
    let user: User
    
    public func getShoppingLists(completion: @escaping ([ShoppingList]) -> Void) {
        
        shoppingListApi.getShoppingListsBy(customerId: user.memberNumber) { (response) in
        switch response {
            case .success(let item):
                completion(item)
                
            case .failure(let error):
                //TODO: Maybe handle this error
                print(error.localizedDescription)
                completion([])
            }
        }
        
    }
}

