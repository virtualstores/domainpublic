//
//  ShoppingList.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-23.
//

import Foundation

public protocol ShoppingLists {
    
    func getShoppingLists(completion: @escaping ([ShoppingList]) -> Void)

}
