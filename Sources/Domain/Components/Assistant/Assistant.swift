//
//  Assistant.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2019-12-11.
//

import Foundation
import CoreGraphics

public protocol AssistantNavigator {
    func closestPointOfInterest(from position: CGPoint, in list: Set<PointOfInterest>) -> PointOfInterest?
}

public class Assistant: Codable {
    // MARK: Properties
    
    public private(set) var pointsOfInterest: Set<PointOfInterest>
    public var navigator: AssistantNavigator?
    public private(set) var pointOfInterestOverride: PointOfInterest?
    private var observers: [String: AssistantObserver] = [:]
    
    private enum CodingKeys: String, CodingKey {
        case pointsOfInterest
    }
    
    public init(navigator: AssistantNavigator? = nil) {
        self.pointsOfInterest = Set()
        self.navigator = navigator
    }
    
    
    // MARK: Setters
    
    public func add(_ assistable: PointOfInterest) {
        if self.pointsOfInterest.contains(assistable) {
            var timestampedAssistable = assistable
            timestampedAssistable.timestamp = Date()
            self.pointsOfInterest.update(with: timestampedAssistable)
            self.notifyObservers(pointsOfInterest: [assistable], state: .edited)
        }
        else {
            var timestampedAssistable = assistable
            timestampedAssistable.timestamp = Date()
            self.pointsOfInterest.insert(timestampedAssistable)
            self.notifyObservers(pointsOfInterest: [assistable], state: .added)
        }
    }
    
    public func add(_ pointsOfInterest: [PointOfInterest]) {
        for poi in pointsOfInterest {
            if self.pointsOfInterest.contains(poi) {
                var timestampedAssistable = poi
                timestampedAssistable.timestamp = Date()
                self.pointsOfInterest.update(with: timestampedAssistable)
            }
            else {
                var timestampedAssistable = poi
                timestampedAssistable.timestamp = Date()
                self.pointsOfInterest.insert(timestampedAssistable)
            }
        }
        
        self.notifyObservers(pointsOfInterest: pointsOfInterest, state: .added)
    }
    
    public func delete(_ assistable: PointOfInterest) {
        if self.pointsOfInterest.remove(assistable) != nil {
            self.notifyObservers(pointsOfInterest: [assistable], state: .removed)
        }
    }
    
    public func delete(with id: String) {
        guard let toRemove = self.pointsOfInterest.first(where: { $0.id == id }) else {
            let removed = self.pointsOfInterest.filter({ $0.alternateIds.contains(id) })
            let keep = self.pointsOfInterest.subtracting(removed)
            self.pointsOfInterest = keep
            if !removed.isEmpty {
                self.notifyObservers(pointsOfInterest: removed.map { $0 }, state: .removed)
            }
            return
        }
        
        if self.pointsOfInterest.remove(toRemove) != nil {
            self.notifyObservers(pointsOfInterest: [toRemove], state: .removed)
        }
    }
    
    public func update(id: String, amount: Double) {
        if var pointOfInterest = pointsOfInterest.first(where: { $0.id == id }), var quantity = pointOfInterest.quantity {
            quantity.amount = amount
            pointOfInterest.quantity = quantity
            pointsOfInterest.update(with: pointOfInterest)
            
            self.notifyObservers(pointsOfInterest: [pointOfInterest], state: .edited)
        }
    }
    
    public func update(id: String, changeInAmount: Double) {
        guard var pointOfInterest = pointsOfInterest.first(where: { $0.id == id }), var quantity = pointOfInterest.quantity else {
            return
        }
        
        quantity.amount += changeInAmount
                
        if quantity.amount <= 0 {
            pointsOfInterest.remove(pointOfInterest)
            self.notifyObservers(pointsOfInterest: [pointOfInterest], state: .removed)
        }
        else {
            pointOfInterest.quantity = quantity
            pointsOfInterest.update(with: pointOfInterest)
            self.notifyObservers(pointsOfInterest: [pointOfInterest], state: .edited)
        }
    }
    
    public func override(with pointOfInterest: PointOfInterest) {
        guard pointOfInterest != pointOfInterestOverride else {
            return
        }
        
        if !contains(pointOfInterest: pointOfInterest) {
            add(pointOfInterest)
        }
        
        pointOfInterestOverride = pointOfInterest
        
        notifyObservers(pointsOfInterest: [pointOfInterest], state: .edited)
    }
    
    public func clearOverride() {
        pointOfInterestOverride = nil
    }

    public func clear() {
        let pointsOfInterestsToRemove = Array(pointsOfInterest)
        pointsOfInterest.removeAll()
        if !pointsOfInterestsToRemove.isEmpty {
            self.notifyObservers(pointsOfInterest: pointsOfInterestsToRemove, state: .removed)
        }
    }
    
    public func clear(type: String) {
        self.clear(types: [type])
    }
    
    public func clear(types: [String]) {
        let pointsOfInterestsWithType = pointsOfInterest.filter { types.contains($0.type) }
        pointsOfInterest = pointsOfInterest.subtracting(pointsOfInterestsWithType)
        if !pointsOfInterestsWithType.isEmpty {
            self.notifyObservers(pointsOfInterest: Array(pointsOfInterestsWithType), state: .removed)
        }
    }
    
    public func clearNotUserAdded() {
        let notAddedByUser = pointsOfInterest.filter { !$0.addedByUser }
        pointsOfInterest = pointsOfInterest.subtracting(notAddedByUser)
        if !notAddedByUser.isEmpty {
            self.notifyObservers(pointsOfInterest: Array(notAddedByUser), state: .removed)
        }
    }
    
    // MARK: Getters
    
    public func get(with id: String) -> PointOfInterest? {
        return pointsOfInterest.first(where: { $0.id == id })
    }
    
    public func getUserAdded() -> Set<PointOfInterest> {
        return pointsOfInterest.filter { $0.addedByUser }
    }
    
    public func contains(id: String) -> Bool {
        return pointsOfInterest.contains(where: { $0.id == id })
    }
    
    public func contains(userAddedId: String) -> Bool {
        return pointsOfInterest.contains(where: { $0.id == userAddedId && $0.addedByUser == true })
    }
    
    public func contains(pointOfInterest: PointOfInterest) -> Bool {
        return pointsOfInterest.contains(pointOfInterest)
    }
    
    public func contains(userAddedPointOfInterest: PointOfInterest) -> Bool {
        return pointsOfInterest.contains(where: { $0 == userAddedPointOfInterest && $0.addedByUser == true })
    }
    
    public func closestPointOfInterest(from point: CGPoint) -> PointOfInterest? {
        if let override = pointOfInterestOverride {
            return override
        }
        
        return navigator?.closestPointOfInterest(from: point, in: self.pointsOfInterest)
    }
    
    
    // MARK: Observers
    
    private func notifyObservers(pointsOfInterest: [PointOfInterest], state: AssistantObserverState) {
        for observer in observers.values {
            observer(pointsOfInterest, state)
        }
    }
    
    public func addObserver(with id: String, observer: @escaping AssistantObserver) {
        observers[id] = observer
    }
    
    public func removeObserver(with id: String) {
        observers[id] = nil
    }
}

public enum AssistantObserverState {
    case added
    case removed
    case edited
}

public typealias AssistantObserver = ([PointOfInterest], AssistantObserverState) -> ()
