//
//  RecepieImpl.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-24.
//

import Foundation

public class RecipeImpl: Recipes {
    public init (recipeApi: RecipeApi){
        self.recipeApi = recipeApi
    }
    
    let recipeApi: RecipeApi
    
    public func getRecipes(completion: @escaping (Result<[Recipe], Error>) -> Void) {
        recipeApi.getRecipes() { (response) in
            switch response {
            case .success(let recipes):
                completion(.success(recipes))
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    public func getRecipesByCategory(category: String, completion: @escaping (Result<[Recipe],Error>) -> Void) {
        //TODO
    }
}
