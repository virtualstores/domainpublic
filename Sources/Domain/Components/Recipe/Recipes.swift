//
//  Recipes.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-24.
//

import Foundation

public protocol Recipes {
    func getRecipes(completion: @escaping (Result<[Recipe],Error>) -> Void)
    func getRecipesByCategory(category: String, completion: @escaping (Result<[Recipe],Error>) -> Void)
}
