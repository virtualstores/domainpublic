//
//  Employee.swift
//  Domain
//
//  Created by Théodore Roos on 2020-08-03.
//

import Foundation

public struct Employee: Codable {
    public let id: Int64
    public let clientId: Int64
    public let name: String
    public let memberNumber: String?
    public let socialSecurity: String?
    public let customerControl: Bool
    
    enum EmployeeCodingKeys: String, CodingKey {
        case id
        case clientId
        case name
        case memberNumber
        case socialSecurity = "unhashedSocialSecurity"
        case customerControl
    }
    
    public init(id: Int64, clientId: Int64, name: String, memberNumber: String?, socialSecurity: String?, customerControl: Bool) {
        self.id = id
        self.clientId = clientId
        self.name = name
        self.memberNumber = memberNumber
        self.socialSecurity = socialSecurity
        self.customerControl = customerControl
    }
}
