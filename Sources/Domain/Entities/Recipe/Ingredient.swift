//
//  Ingredient.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-24.
//

import Foundation

public class Ingredient: Codable {
    public let name: String
    public let unit: String
    public let optional: Bool
    public let quantity: [Double]
    public let item: RecipeItem?
    
    enum ItemCodingKeys: String, CodingKey {
        case name
        case unit
        case optional
        case quantity
        case item
    }
    
    public init(name: String, unit: String, optional: Bool, quantity: [Double], item: RecipeItem?) {
        self.name = name
        self.unit = unit
        self.optional = optional
        self.quantity = quantity
        self.item = item
    }
    
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ItemCodingKeys.self)
        
        name = try container.decode(String.self, forKey: .name)
        unit = try container.decode(String.self, forKey: .unit)
        optional = try container.decode(Bool.self, forKey: .optional)
        quantity = try container.decode([Double].self, forKey: .quantity)
        item = try container.decode(RecipeItem.self, forKey: .item)
    }
}
