//
//  IngredientSection.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-24.
//

import Foundation

public class IngredientSection: Codable {
    public let sectionName: String?
    public let ingredients: [Ingredient]
    
    enum ItemCodingKeys: String, CodingKey {
        case sectionName
        case ingredients
    }
    
    public init(sectionName: String? = nil, ingredients: [Ingredient]) {
        self.sectionName = sectionName
        self.ingredients = ingredients
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ItemCodingKeys.self)
        
        sectionName = try container.decode(String.self, forKey: .sectionName)
        ingredients = try container.decode([Ingredient].self, forKey: .ingredients)
    }
}
