//
//  RecipeItem.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-24.
//

import Foundation

public class RecipeItem: Assistable, BarcodeIdentifiable {
    public let barcode: String
    public let code: String
    public let imageUrl: String?
    public let name: String
    public let unit: String
    public let shelfTierIds: [Int64]
    public let quantity: [Double]
    
    public static var Navigation: Navigation?
    
    enum ItemCodingKeys: String, CodingKey {
        case barcode
        case code
        case imageUrl
        case name
        case unit
        case shelfTierIds
        case quantity
    }
    
    public var asPointOfInterest: PointOfInterest {
        let position = RecipeItem.Navigation?.getItemPosition(shelfTierId: shelfTierIds.first ?? 0)
        return PointOfInterest(id: barcode, alternateIds: [], name: name, subtitle: "", description: "", quantity: PointOfInterest.Quantity(amount: quantity.first ?? 1.0, unit: UnitOfMeasure.init(fromLocalizedString: unit)), position: position, addedByUser: true, imageUrl: URL(string: imageUrl ?? ""), type: GroceryStorePointOfInterest.recipeItem)
    }
    
    public init(barcode: String, code: String, imageUrl: String? = nil, name: String, unit: String, shelfTierIds: [Int64], quantity: [Double]) {
        self.barcode = barcode
        self.code = code
        self.imageUrl = imageUrl
        self.name = name
        self.unit = unit
        self.shelfTierIds = shelfTierIds
        self.quantity = quantity
        
        super.init()
        self.type = .recipe
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ItemCodingKeys.self)
        
        barcode = try container.decode(String.self, forKey: .barcode)
        code = try container.decode(String.self, forKey: .code)
        imageUrl = try container.decode(String.self, forKey: .imageUrl)
        name = try container.decode(String.self, forKey: .name)
        unit = try container.decode(String.self, forKey: .unit)
        shelfTierIds = try container.decode([Int64].self, forKey: .shelfTierIds)
        
        quantity = try container.decode([Double].self, forKey: .quantity)
        let superDecoder = try container.superDecoder()
        try super.init(from: superDecoder)
    }
    
    // TODO: Better way to compare items and recipe items
    public func asQuantifiableItem() -> QuantifiableItem {
        return QuantifiableItem(item: Item(id: self.code, name: self.name, prettyName: self.name, description: "", manufacturerName: "", imageUrl: self.imageUrl, alternateIds: nil, ecoLabels: nil, shelfTierIds: self.shelfTierIds, offerShelfTierIds: nil, offerItemId: nil, price: nil), quantity: self.quantity[0], unit: UnitOfMeasure(fromLocalizedString: unit))
    }
    
     // Assistable properties

    func match(barcode: String) -> Bool {
        return self.barcode == barcode
    }
}
