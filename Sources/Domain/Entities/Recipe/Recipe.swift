//
//  Recipe.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-24.
//

import Foundation

public class Recipe {
    public let id: Int
    public let recipeId: String
    public let name: String
    public let imageUrl: String
    public let recipeUrl: String
    public let cookingTimeInMinutes: Int
    public let numberOfMeals: [Int]
    public let categories: [String]
    public let allergies: [String]?
    public let cookingInstruction: [String]
    public let ingredientSections: [IngredientSection]
    
    enum ItemCodingKeys: String, CodingKey {
        case id
        case recipeId
        case name
        case imageUrl
        case recipeUrl
        case cookingTimeInMinutes
        case numberOfMeals
        case categories
        case allergies
        case cookingInstruction
        case ingredientSections
    }
    
    public init(id: Int, recipeId: String, name: String, imageUrl: String, recipeUrl: String, cookingTimeInMinutes: Int, numberOfMeals: [Int], categories: [String], allergies: [String]?, cookingInstruction: [String], ingredientSections: [IngredientSection]) {
        self.id = id
        self.recipeId = recipeId
        self.name = name
        self.imageUrl = imageUrl
        self.recipeUrl = recipeUrl
        self.cookingTimeInMinutes = cookingTimeInMinutes
        self.numberOfMeals = numberOfMeals
        self.categories = categories
        self.allergies = allergies
        self.cookingInstruction = cookingInstruction
        self.ingredientSections = ingredientSections
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ItemCodingKeys.self)
        
        id = try container.decode(Int.self, forKey: .id)
        recipeId = try container.decode(String.self, forKey: .recipeId)
        name = try container.decode(String.self, forKey: .name)
        imageUrl = try container.decode(String.self, forKey: .imageUrl)
        recipeUrl = try container.decode(String.self, forKey: .recipeUrl)
        cookingTimeInMinutes = try container.decode(Int.self, forKey: .cookingTimeInMinutes)
        numberOfMeals = try container.decode([Int].self, forKey: .numberOfMeals)
        categories = try container.decode([String].self, forKey: .categories)
        allergies = try container.decode([String].self, forKey: .allergies)
        cookingInstruction = try container.decode([String].self, forKey: .cookingInstruction)
        ingredientSections = try container.decode([IngredientSection].self, forKey: .ingredientSections)
    }

}
