//
//  Offer.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2019-12-16.
//

import Foundation

public class Offer: Assistable, BarcodeIdentifiable {
    
    public enum OfferType: Int, Codable {
        case general = 0
        case loyalty = 1
        case special = 2
    }
    
    public let title: String
    public let offerPrice: Double
    public let offerPriceUnitOfMeasure: String
    public let originalPrice: Double
    public let originalPriceUnitOfMeasure: String
    public let sortPosition: Double
    public let imageUrl: String
    public let brand: String
    public let decorator: String?
    public let redeemLimitLabel: String?
    public let volume: String?
    public let startDate: String
    public let endDate: String
    public let offerType: OfferType
    public let ecoLabels: [EcoLabel]
    public let products: [Item]
    public let conditionLabel: String?
    
    public static var Navigation: Navigation?

    public var asPointOfInterest: PointOfInterest? {
        guard let item = products.first else {
            return nil
        }
        
        let poiType: String
        switch offerType {
        case .general:
            poiType = StorePointOfInterest.offer
        case .loyalty:
            poiType = StorePointOfInterest.loyaltyOffer
        case .special:
            poiType = StorePointOfInterest.specialOffer
        }
        
        let position = Offer.Navigation?.getItemPosition(shelfTierId: item.offerShelfTierIds?.first ?? 0)
        
        let subtitle: String
        if !brand.isEmpty, let volume = volume, !volume.isEmpty {
            subtitle = "\(brand), \(volume)"
        }
        else if !brand.isEmpty {
            subtitle = "\(brand)"
        }
        else if let volume = volume, !volume.isEmpty {
            subtitle = "\(volume)"
        }
        else {
            subtitle = " "
        }
        
        let description: String
        if let condition = conditionLabel {
            description = "\(condition)\n\(String(format: "%.2f", offerPrice)) \(offerPriceUnitOfMeasure)"
        }
        else {
            description = "\(String(format: "%.2f", offerPrice))\n\(offerPriceUnitOfMeasure)"
        }
        
        return PointOfInterest(id: item.offerItemId ?? "", alternateIds: item.alternateIds?.map { $0.id } ?? [], name: title, subtitle: subtitle, description: description, quantity: nil, position: position, addedByUser: false, imageUrl: URL(string: imageUrl), type: poiType)
    }
    
    enum OfferCodingKeys: String, CodingKey {
        case unitPrice
        case unitOfMeasure
        case title
        case offerPrice
        case offerPriceUnitOfMeasure
        case originalPrice
        case originalPriceUnitOfMeasure
        case sortPosition
        case imageUrl
        case brand
        case decorator
        case redeemLimitLabel
        case volume
        case startDate
        case endDate
        case offerType
        case ecoLabels
        case products
        case conditionLabel
    }
    
    public init(title: String, offerPrice: Double, offerPriceUnitOfMeasure: String, originalPrice: Double, originalPriceUnitOfMeasure: String, sortPosition: Double, imageUrl: String, brand: String, decorator: String?, redeemLimitLabel: String?, volume: String?, startDate: String, endDate: String, offerType: OfferType, ecoLabels: [EcoLabel], products: [Item], conditionLabel: String?) {
        self.title = title
        self.offerPrice = offerPrice
        self.offerPriceUnitOfMeasure = offerPriceUnitOfMeasure
        self.originalPrice = originalPrice
        self.originalPriceUnitOfMeasure = originalPriceUnitOfMeasure
        self.sortPosition = sortPosition
        self.imageUrl = imageUrl
        self.brand = brand
        self.decorator = decorator
        self.redeemLimitLabel = redeemLimitLabel
        self.volume = volume
        self.startDate = startDate
        self.endDate = endDate
        self.offerType = offerType
        self.ecoLabels = ecoLabels
        self.products = products
        self.conditionLabel = conditionLabel
        super.init()
        self.type = .offer
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: OfferCodingKeys.self)
        
        title = try container.decode(String.self, forKey: .title)
        offerPrice = try container.decode(Double.self, forKey: .offerPrice)
        offerPriceUnitOfMeasure = try container.decode(String.self, forKey: .offerPriceUnitOfMeasure)
        originalPrice = try container.decode(Double.self, forKey: .originalPrice)
        originalPriceUnitOfMeasure = try container.decode(String.self, forKey: .originalPriceUnitOfMeasure)
        sortPosition = try container.decode(Double.self, forKey: .sortPosition)
        imageUrl = try container.decode(String.self, forKey: .imageUrl)
        brand = try container.decode(String.self, forKey: .brand)
        decorator = try container.decode(String.self, forKey: .decorator)
        redeemLimitLabel = try container.decode(String.self, forKey: .redeemLimitLabel)
        volume = try container.decode(String.self, forKey: .volume)
        startDate = try container.decode(String.self, forKey: .startDate)
        endDate = try container.decode(String.self, forKey: .endDate)
        offerType = try container.decode(OfferType.self, forKey: .offerType)
        ecoLabels = try container.decode([EcoLabel].self, forKey: .ecoLabels)
        products = try container.decode([Item].self, forKey: .products)
        conditionLabel = try container.decode(String.self, forKey: .conditionLabel)
        
        let superDecoder = try container.superDecoder()
        try super.init(from: superDecoder)
        
        self.type = .offer
    }
    
    // MARK: Assistable properties
    
    override public func getItemPositions() -> [ItemPosition] {
        var positions: [ItemPosition] = []
        for item in products {
            Item.Navigation = Offer.Navigation
            positions.append(contentsOf: item.getItemPositions())
        }
        return positions
    }
    
    override func isEqual(assistable: Assistable) -> Bool {
        if assistable is Offer {
            let offer = assistable as! Offer
            return (self.title == offer.title) && self.offerPrice == offer.offerPrice
        }
        else if assistable is Item {
            let item = assistable as! Item
            return products.contains { product in
                return product.id == item.id
            }
        }
        
        return false
    }
    
    override func clone() -> Assistable {
        return cloneParentFields(assistable: Offer(title: self.title, offerPrice: self.offerPrice, offerPriceUnitOfMeasure: self.offerPriceUnitOfMeasure, originalPrice: self.originalPrice, originalPriceUnitOfMeasure: self.originalPriceUnitOfMeasure, sortPosition: self.sortPosition, imageUrl: self.imageUrl, brand: self.brand, decorator: self.decorator, redeemLimitLabel: self.redeemLimitLabel, volume: self.volume, startDate: self.startDate, endDate: self.endDate, offerType: self.offerType, ecoLabels: self.ecoLabels, products: self.products, conditionLabel: self.conditionLabel))
    }
    
    // MARK: BarcodeIdentifiable properties
    
    func match(barcode: String) -> Bool {
        for product in self.products {
            if product.match(barcode: barcode) { return true }
        }
        return false
    }
}
