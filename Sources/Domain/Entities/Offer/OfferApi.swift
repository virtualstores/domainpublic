//
//  OfferApi.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-20.
//

import Foundation


public protocol OfferApi {
    func getOffersBy(customerId: String, completion: @escaping (Result<[Offer], ApiError>) -> Void)
}
