//
//  PositionedCode.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-04-05.
//

import CoreGraphics

public struct PositionedCode: Codable {
    public let code: String
    public let coordinates: CGPoint?
    public let direction: Double?
    
    public init(code: String, coordinates: CGPoint? = nil, direction: Double? = nil) {
        self.code = code
        self.coordinates = coordinates
        self.direction = direction
    }
}
