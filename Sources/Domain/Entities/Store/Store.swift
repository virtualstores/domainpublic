//
// Created by Carl-Johan Dahlman on 2019-10-11.
//

import Foundation



public struct Store: Codable {
    
    public struct RTLS: Codable {
        public let width: Int64
        public let height: Int64
        public let startOffsetX: Int64
        public let startOffsetY: Int64
        public let mapBoxUrl: String?
        public let mapBoxToken: String?
        public let mapFenceUrl: String?
        public let mapZonesUrl: String?
        public let navGraphUrl: String?
        public let offsetZonesUrl: String?
        public let mapDataVersionUrl: String?
        
        public init(width: Int64, height: Int64, startOffsetX: Int64, startOffsetY: Int64, mapBoxUrl: String, mapBoxToken: String, mapFenceUrl: String, mapZonesUrl: String, navGraphUrl: String, offsetZonesUrl: String, mapDataVersionUrl: String) {
            self.width = width
            self.height = height
            self.startOffsetX = startOffsetX
            self.startOffsetY = startOffsetY
            self.mapBoxUrl = mapBoxUrl
            self.mapBoxToken = mapBoxToken
            self.mapFenceUrl = mapFenceUrl
            self.mapZonesUrl = mapZonesUrl
            self.navGraphUrl = navGraphUrl
            self.offsetZonesUrl = offsetZonesUrl
            self.mapDataVersionUrl = mapDataVersionUrl
        }
    }
    
    public struct Address: Codable {
        public let city: String?
        public let zipCode: String?
        public let address: String?
        public let description: String?
        
        public init(city: String?, zipCode: String?, address: String?, description: String?) {
            self.city = city
            self.zipCode = zipCode
            self.address = address
            self.description = description
        }
    }
    
    public enum PaymentMethod: Int, Codable {
        case payExCard = 1
        case payExSwish = 2
        case klarna = 3
        case selfCheckout = 4
        case registry = 5
    }
    
    public let id: Int64
    public let name: String
    public let address: Address
    public let latitude: Double
    public let longitude: Double
    public let active: Bool
    public let hasMap: Bool
    public let startCodes: [PositionedCode]
    public let stopCodes: [PositionedCode]
    public let connection: ServerConnection
    public let rtls: RTLS
    public let paymentMethods: [Store.PaymentMethod]
    public let minVersion: String
    

    public init(id: Int64, name: String, address: Address, latitude: Double, longitude: Double, active: Bool, hasMap: Bool, startCodes: [PositionedCode], stopCodes: [PositionedCode], connection: ServerConnection, rtls: RTLS, paymentMethods: [Store.PaymentMethod], minVersion: String) {
		self.id = id
		self.name = name
		self.address = address
		self.latitude = latitude
		self.longitude = longitude
        self.active = active
        self.hasMap = hasMap
        self.startCodes = startCodes
        self.stopCodes = stopCodes
		self.connection = connection
        self.rtls = rtls
        self.paymentMethods = paymentMethods
        self.minVersion = minVersion
	}
}

