import Foundation


public struct ServerConnection: Codable {
    
    public let id: Int64
    public let storeId: Int64
    public let serverUrl: String
    public let mqttUrl: String
    public let apiKey: String
	
	public init(id: Int64, storeId: Int64, serverUrl: String, mqttUrl: String, apiKey: String) {
		self.id = id
		self.storeId = storeId
		self.serverUrl = serverUrl
		self.mqttUrl = mqttUrl
		self.apiKey = apiKey
	}
}
