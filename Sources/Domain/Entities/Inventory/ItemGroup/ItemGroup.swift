//
//  ItemGroup.swift
//  domain
//
//  Created by Carl-Johan Dahlman on 2019-10-15.
//

public struct ItemGroupDetail: Codable {
    enum CodingKeys: String, CodingKey {
        case subgroups = "itemGroups"
        case items
    }
    
    public let subgroups: [ItemGroup]
    public let items: [Item]
    
    public init(subgroups: [ItemGroup], items: [Item]) {
        self.subgroups = subgroups
        self.items = items
    }
}

public struct ItemGroup: Codable {
    
    public let id: String
    public let name: String
    public let prettyName: String
    public let imageUrl: String?
    public let itemPosition: ItemPosition?
    public let parentItemGroupId: String?
	
    public init(id: String, name: String, prettyName: String, imageUrl: String?, itemPosition: ItemPosition?, parentItemGroupId: String?) {
		self.id = id
		self.name = name
		self.prettyName = prettyName
		self.imageUrl = imageUrl
        self.itemPosition = itemPosition
        self.parentItemGroupId = parentItemGroupId
	}
}
