//
//  Cancellable.swift
//  Domain
//
//  Created by Philip Fryklund on 2019-11-16.
//

import Foundation


public protocol Cancellable {
	
	func cancel()
}
