//
//  SearchApi.swift
//  domain
//
//  Created by Carl-Johan Dahlman on 2019-10-15.
//

import Foundation


public protocol SearchApi {
	
    func findBy(withExtension: Bool, searchvalue: String, onlyWithPosition: Bool?, itemGroupId: String?, limit: Int?, completion: @escaping (Result<SearchResult, Error>) -> Void) -> Cancellable
}
