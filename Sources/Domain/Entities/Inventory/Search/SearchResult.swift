//
//  SearchResult.swift
//  domain
//
//  Created by Carl-Johan Dahlman on 2019-10-15.
//

import Foundation


public struct SearchResult {
    
    public let itemGroups: [ItemGroup]
    public let items: [Item]
	
	public init(itemGroups: [ItemGroup], items: [Item]) {
		self.itemGroups = itemGroups
		self.items = items
	}
}
