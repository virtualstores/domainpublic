//
//  EcoLabel.swift
//  domain

import Foundation


public struct EcoLabel: Codable {
    
    public let id: Int64?
    public let name: String?
    public let prettyName: String?
    public let imageUrl: String?
	
	public init(id: Int64?, name: String?, prettyName: String?, imageUrl: String?) {
		self.id = id
		self.name = name
		self.prettyName = prettyName
		self.imageUrl = imageUrl
	}
}
