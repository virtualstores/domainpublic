//
//  Item.swift
//  domain
//
//  Created by Carl-Johan Dahlman on 2019-10-14.
//

import Foundation
import UIKit

open class Item: Assistable, BarcodeIdentifiable {
    public let id: String
    public let name: String
    public let prettyName: String
    public let description: String
    public let manufacturerName: String
    public let imageUrl: String?
    public let alternateIds: [AlternateId]?
    public let ecoLabels: [EcoLabel]?
    public let shelfTierIds: [Int64]?
    public let offerShelfTierIds: [Int64]?
    public let offerItemId: String?
    public let price: Double?
    public let position: ItemPosition?
    public static var Navigation: Navigation?
    
    public var asPointOfInterest: PointOfInterest {
        let position = Item.Navigation?.getItemPosition(shelfTierId: shelfTierIds?.first ?? 0) ?? self.position
        
        let ids = (alternateIds?.map { $0.id } ?? []) + (alternateIds?.compactMap { $0.itemId } ?? [])
        
        return PointOfInterest(id: id, alternateIds: ids, name: name, subtitle: manufacturerName, description: "", quantity: nil, position: position, addedByUser: true, imageUrl: URL(string: imageUrl ?? ""), type: StorePointOfInterest.searchItem)
    }
    
    enum ItemCodingKeys: String, CodingKey {
        case id
        case name
        case prettyName
        case description
        case manufacturerName
        case imageUrl
        case alternateIds
        case ecoLabels
        case shelfTierIds
        case offerShelfTierIds
        case offerItemId
        case price
    }
    
    public init(id: String, name: String, prettyName: String, description: String, manufacturerName: String, imageUrl: String?, alternateIds: [AlternateId]?, ecoLabels: [EcoLabel]?, shelfTierIds: [Int64]?, offerShelfTierIds: [Int64]?, offerItemId: String?, price: Double?, position: ItemPosition? = nil) {
        self.id = id
		self.name = name
		self.prettyName = prettyName
		self.description = description
		self.manufacturerName = manufacturerName
		self.imageUrl = imageUrl
		self.alternateIds = alternateIds
		self.ecoLabels = ecoLabels
		self.shelfTierIds = shelfTierIds
        self.offerShelfTierIds = offerShelfTierIds
        self.offerItemId = offerItemId
        self.price = price
        self.position = position
        
        super.init()
        
        self.type = .item
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ItemCodingKeys.self)
        
        id = try container.decode(String.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        prettyName = try container.decode(String.self, forKey: .prettyName)
        description = try container.decode(String.self, forKey: .description)
        manufacturerName = try container.decode(String.self, forKey: .manufacturerName)
        imageUrl = try container.decode(String.self, forKey: .imageUrl)
        alternateIds = try container.decode([AlternateId].self, forKey: .alternateIds)
        ecoLabels = try container.decode([EcoLabel].self, forKey: .ecoLabels)
        shelfTierIds = try container.decode([Int64].self, forKey: .shelfTierIds)
        offerShelfTierIds = try container.decode([Int64].self, forKey: .offerShelfTierIds)
        offerItemId = try container.decode(String.self, forKey: .offerItemId)
        price = try container.decode(Double.self, forKey: .price)
        position = nil
        
        let superDecoder = try container.superDecoder()
        try super.init(from: superDecoder)
    }
    
    func match(barcode: String) -> Bool {
        return id == barcode || alternateIds?.contains(where: {
            return $0.itemId == barcode
        }) ?? false
    }
}

public extension Item {
    convenience init(id: String, name: String, imageUrl: String?, position: ItemPosition) {
        self.init(id: id, name: name, prettyName: "", description: "", manufacturerName: "", imageUrl: imageUrl, alternateIds: nil, ecoLabels: nil, shelfTierIds: nil, offerShelfTierIds: nil, offerItemId: nil, price: nil, position: position)
    }
}
