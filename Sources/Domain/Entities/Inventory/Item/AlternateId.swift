//
//  AlternateId.swift
//  domain
//
//  Created by Carl-Johan Dahlman on 2019-10-15.
//

import Foundation


public struct AlternateId: Codable {
	
	public enum IdType: Int, Codable {
		case gtin = 0
		case plu = 1
		case sku = 2
		case isbn = 3
		case issn = 4
		case ean13 = 5
		case ean8 = 6
		case ean128 = 7
		case itemId = 8
		case unknown = 9
	}
    
    public let id: String
    public let itemId: String?
    public let type: IdType
    
    public init(id: String, itemId: String?, type: IdType) {
        self.id = id
        self.itemId = itemId
        self.type = type
    }
}
