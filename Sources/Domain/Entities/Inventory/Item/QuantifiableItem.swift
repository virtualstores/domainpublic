//
//  QuantifiableItem.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-01-21.
//

import Foundation

open class QuantifiableItem: Item {
    public var quantity: Double
    public var unit: UnitOfMeasure
    
    enum QuantifiableItemCodingKeys: String, CodingKey {
        case quantity
        case unit
    }
    
    public init(item: Item, quantity: Double, unit: UnitOfMeasure) {
        self.quantity = quantity
        self.unit = unit
        
        super.init(id: item.id, name: item.name, prettyName: item.prettyName, description: item.description, manufacturerName: item.manufacturerName, imageUrl: item.imageUrl, alternateIds: item.alternateIds, ecoLabels: item.ecoLabels, shelfTierIds: item.shelfTierIds, offerShelfTierIds: item.offerShelfTierIds, offerItemId: item.offerItemId, price: item.price)
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: QuantifiableItemCodingKeys.self)
        
        quantity = try container.decode(Double.self, forKey: .quantity)
        unit = try container.decode(UnitOfMeasure.self, forKey: .unit)
        let superDecoder = try container.superDecoder()
        try super.init(from: superDecoder)
    }
    
    // MARK: Assistable properties
    
    override func isEqual(assistable: Assistable) -> Bool {
        if assistable is QuantifiableItem {
            let item = assistable as! QuantifiableItem
            return self.id == item.id
        }
        
        return false
    }
    
    override func clone() -> Assistable {
        return cloneParentFields(assistable: QuantifiableItem(item: Item(id: self.id, name: self.name, prettyName: self.prettyName, description: self.description, manufacturerName: self.manufacturerName, imageUrl: self.imageUrl, alternateIds: self.alternateIds, ecoLabels: self.ecoLabels, shelfTierIds: self.shelfTierIds, offerShelfTierIds: self.offerShelfTierIds, offerItemId: self.offerItemId, price: self.price), quantity: self.quantity, unit: self.unit))
    }
}
