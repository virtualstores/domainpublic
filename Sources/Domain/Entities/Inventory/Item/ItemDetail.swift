//
//  ItemDetail.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-05-20.
//

public struct ItemDetail: Codable {
    public struct Origin: Codable {
        public let country: String?
        public let manufacturingCountry: String?
    }
    
    public struct Warnings: Codable {
        public let hazards: [String]
        public let precautions: [String]
    }
    
    public struct Instructions: Codable {
        public let general: String?
        public let usage: String?
        public let storage: String?
        public let preparation: String?
    }
    
    public struct StorageInformation: Codable {
        public let minTemperature: String?
        public let maxTemperature: String?
    }
    
    public struct NutritionalInformation: Codable {
        public struct Row: Codable {
            public let type: String
            public let value: String
            public let unitOfMeasure: String
            public let measurementPrecision: String?
            
            public init(type: String, value: String, unitOfMeasure: String, measurementPrecision: String?) {
                self.type = type
                self.value = value
                self.unitOfMeasure = unitOfMeasure
                self.measurementPrecision = measurementPrecision
            }
        }
        
        public let description: String
        public let nutritions: [Row]
        
        public init(description: String, nutritions: [Row]) {
            self.description = description
            self.nutritions = nutritions
        }
    }
    
    public struct Group: Codable {
        public let itemGroupId: String
        public let itemId: String
    }
    
    public struct Shelf: Codable {
        public let shelfTierId: Int
        public let itemId: String
    }

    public let id: String
    public let name: String
    public let prettyName: String
    public let description: String?
    public let manufacturerName: String?
    public let supplierName: String?
    public let vat: Double
    public let price: Double
    public let imageUrl: String
    public let volume: String
    public let ingredientsDescription: String?
    public let action: Int
    public let origin: Origin?
    public let warnings: Warnings?
    public let instructions: Instructions?
    public let storageInformation: StorageInformation?
    public let nutritionInformation: NutritionalInformation?
    public let additionalDescriptions: [String]
    public let alternateIds: [AlternateId]
    public let itemGroupItems: [Group]?
    public let ecoLabels: [EcoLabel]
    public let itemPositions: [ItemPosition]?
    public let shelfTierItemRelations: [Shelf]
    
    public init(id: String, name: String, description: String, supplierName: String?, price: Double, imageUrl: String, volume: String, ingredientsDescription: String?, nutritionInformation: NutritionalInformation?) {
        self.id = id
        self.name = name
        self.prettyName = name
        self.description = description
        self.manufacturerName = supplierName
        self.supplierName = supplierName
        self.vat = 0.0
        self.price = price
        self.imageUrl = imageUrl
        self.volume = volume
        self.ingredientsDescription = ingredientsDescription
        self.action = 0
        self.origin = Origin(country: nil, manufacturingCountry: nil)
        self.warnings = Warnings(hazards: [], precautions: [])
        self.instructions = Instructions(general: nil, usage: nil, storage: nil, preparation: nil)
        self.storageInformation = StorageInformation(minTemperature: "1000", maxTemperature: "10000")
        self.nutritionInformation = nutritionInformation
        self.additionalDescriptions = []
        self.alternateIds = []
        self.itemGroupItems = []
        self.ecoLabels = []
        self.itemPositions = []
        self.shelfTierItemRelations = []
    }
}
