//
//  ItemsApi.swift
//  domain
//
//  Created by Carl-Johan Dahlman on 2019-10-15.
//

import Foundation


public protocol ItemApi {
	
    func getItemBy(id: String, completion: @escaping (Result<Item, Error>) -> Void)
    
    func getRecommendedItems(for memberId: String, completion: @escaping (Result<[Item], Error>) -> Void)
    
    func getItemGroup(limit: Int32, offset: Int32, completion: @escaping (Result<[ItemGroup], Error>) -> Void)

    func getItemGroupDetail(id: String, completion: @escaping (Result<ItemGroupDetail, Error>) -> Void)
    
    func getItemDetail(id: String, completion: @escaping (Result<ItemDetail, Error>) -> Void)

}
