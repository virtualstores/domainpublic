//
//  PaymentInformation.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-02-27.
//

import Foundation

public enum PaymentMethod: Int, Codable {
    case payExCard = 0
    case payExSwish = 1
    case klarna = 2
    case selfCheckout = 3
    case register = 4
    case none = 5
}

public struct PaymentInformation: Codable {
    public struct CardResource: Codable {
        public var paymentId: String?
        public var hostUrl: String?
    }

    public struct SwishResource: Codable {
        public var token: String?
        public var paymentId: String?
    }
    
    public var payMethod: PaymentMethod
    public var payExCardResource: CardResource?
    public var payExSwishResource: SwishResource?
}
