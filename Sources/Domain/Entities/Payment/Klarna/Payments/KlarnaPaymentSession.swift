import Foundation

public struct KlarnaPaymentSession {
    
    public var sessionId: String
    public var clientToken: String
    public var paymentMethodCategories: [PaymentMethodCategory]
    
    public init(sessionId: String, clientToken: String, paymentMethodCategories: [PaymentMethodCategory]) {
        self.sessionId = sessionId
        self.clientToken = clientToken
        self.paymentMethodCategories = paymentMethodCategories
    }
    
    public struct PaymentMethodCategory {
        public var identifier: String
        public var name: String
        public var assetUrls: AssetUrls
        
        public init(identifier: String, name: String, assetUrls: AssetUrls) {
            self.identifier = identifier
            self.name = name
            self.assetUrls = assetUrls
        }
    }
    
    public struct AssetUrls {
        public var descriptive: String
        public var standard: String
        
        public init(descriptive: String, standard: String){
            self.descriptive = descriptive
            self.standard = standard
        }
    }
    
}
