import Foundation

public protocol KlarnaPaymentsApi {
    func start(sessionId: Int64, cartId: String, completion: @escaping (Result<KlarnaPaymentSession, Error>) -> Void)
    
    func complete(sessionId: Int64, cartId: String, authToken: String, completion: @escaping (Result<Any, Error>) -> Void)
    
    func cancel(sessionId: Int64, cartId: String, completion: @escaping (Result<Any, Error>) -> Void)
}
