import Foundation

public struct PayExVerification: Decodable {
    
    public let token: String?
    public let maskedCardNumber: String?
    public let cardBrand: String?
    
    public init (token: String?, maskedCardNumber: String?, cardBrand: String?){
        self.token = token
        self.maskedCardNumber = maskedCardNumber
        self.cardBrand = cardBrand
    }
}

public class PayExUrls: Decodable {
	public let hostUrls: [URL]
    public let completeUrl: URL
	public let cancelUrl: URL
	public let paymentUrl: URL
	public let callbackUrl: URL
	public let logoUrl: URL
	public let termsOfServiceUrl: URL?
	
	public init(hostUrls: [URL], completeUrl: URL, cancelUrl: URL, paymentUrl: URL, callbackUrl: URL, logoUrl: URL, termsOfServiceUrl: URL?) {
		self.hostUrls = hostUrls
		self.completeUrl = completeUrl
		self.cancelUrl = cancelUrl
		self.paymentUrl = paymentUrl
		self.callbackUrl = callbackUrl
		self.logoUrl = logoUrl
		self.termsOfServiceUrl = termsOfServiceUrl
	}
}

public struct PayExPayment: Decodable {
    public let urls: PayExUrls
	public let _id: String
	
	public init(urls: PayExUrls, _id: String) {
        self.urls = urls
		self._id = _id
    }
	
	enum CodingKeys: String, CodingKey {
		case urls
		case _id = "id"
	}
}

public struct PayExStartResponse: Decodable {
    public let payment: PayExPayment
    
    public init(payment: PayExPayment){
        self.payment = payment
    }
}

public protocol PayExCardPaymentsApi {
    
    func start(sessionId: Int64, cartId: String, generateToken: Bool?, paymentToken: String?, completion: @escaping (Result<PayExStartResponse, Error>) -> Void)
    
    func complete(sessionId: Int64, cartId: String, completion: @escaping (Result<Any, Error>) -> Void)
    
    func verify(sessionId: Int64, cartId: String, completion: @escaping (Result<PayExVerification, Error>) -> Void)
	
	func payexCallback(sessionId: Int64, cartId: String, paymentId: String, completion: @escaping (Result<Any, Error>) -> Void)
    
    func cancel(sessionId: Int64, cartId: String, completion: @escaping (Result<Any, Error>) -> Void)
}
