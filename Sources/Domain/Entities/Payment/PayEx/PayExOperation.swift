import Foundation


public struct PayExOperation: Decodable {
    
     public let href: String
     public let rel: String
     public let method: String
     public let contentType: String?
     
     public init (href: String, rel: String, method: String, contentType: String?){
         self.href = href
         self.rel = rel
         self.method = method
         self.contentType = contentType ?? ""
     }
}
