//
//  PayExSwishStartResponse.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-03-24.
//

import Foundation

public struct PayExSwishStartResponse: Decodable {
    public let paymentId: String
    public let transactionId: String
    public let operation: PayExOperation
    
    public init(paymentId: String, transactionId: String, operation: PayExOperation) {
        self.paymentId = paymentId
        self.transactionId = transactionId
        self.operation = operation
    }
}
