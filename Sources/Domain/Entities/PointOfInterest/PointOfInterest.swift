//
//  PointOfInterest.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-02-09.
//

import Foundation

public enum StorePointOfInterest {
    public static let searchItem = "store/search-item"
    public static let shoppingListItem = "store/shopping-list-item"
    public static let offer = "store/offer"
    public static let loyaltyOffer = "store/loyalty-offer"
    public static let specialOffer = "store/special-offer"
}

public enum GroceryStorePointOfInterest {
    public static let recipeItem = "store/grocery/recipe-item"
}


public struct PointOfInterest: Hashable, Equatable, Codable {
    public static let message = "message"
    
    public struct Quantity: Codable {
        public var amount: Double
        public let unit: UnitOfMeasure
    }
    
    public let id: String
    public let alternateIds: [String]
    public var name: String
    public var subtitle: String
    public var description: String
    public var quantity: Quantity?
    public var position: ItemPosition?
    public var addedByUser: Bool
    public var imageUrl: URL?
    public var type: String
    public internal(set) var timestamp: Date? = nil
    
    public static func == (lhs: PointOfInterest, rhs: PointOfInterest) -> Bool {
        return lhs.id == rhs.id
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
