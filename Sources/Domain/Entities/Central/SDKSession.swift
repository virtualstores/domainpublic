//
//  SDKSession.swift
//  Domain
//
//  Created by Emil Bond on 2020-12-09.
//

import Foundation

public struct SDKSession {
    let sessionId: String
    let device: DeviceInformation
    
    public init(sessionId: String, device: DeviceInformation) {
        self.sessionId = sessionId
        self.device = device
    }
}
