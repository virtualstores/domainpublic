//
//  Order.swift
//  Domain
//
//  Created by Emil Bond on 2020-12-09.
//

import Foundation

public struct Order {
    let storeId :Int
    let orderIds: [String]
    let device: DeviceInformation
    
    public init(storeId: Int, orderIds: [String], device: DeviceInformation) {
        self.storeId = storeId
        self.orderIds = orderIds
        self.device = device
    }
}
