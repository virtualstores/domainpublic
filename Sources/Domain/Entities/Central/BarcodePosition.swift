//
//  BarcodePosition.swift
//  Domain
//
//  Created by Emil Bond on 2020-12-04.
//

import Foundation

public struct BarcodePosition: Codable {
    public var shelfId: Int64?
    public var shelfTierId: Int64?
    public var itemPositionX: Double?
    public var itemPositionY: Double?
    public var itemPositionOffsetX: Double?
    public var itemPositionOffsetY: Double?
    public var barcode: String?

    public init(shelfId: Int64?, shelfTierId: Int64?, itemPositionX: Double?, itemPositionY: Double?, itemPositionOffsetX: Double?, itemPositionOffsetY: Double?, barcode: String?) {
        self.shelfId = shelfId
        self.shelfTierId = shelfTierId
        self.itemPositionX = itemPositionX
        self.itemPositionY = itemPositionY
        self.itemPositionOffsetX = itemPositionOffsetX
        self.itemPositionOffsetY = itemPositionOffsetY
        self.barcode = barcode
    }
}
