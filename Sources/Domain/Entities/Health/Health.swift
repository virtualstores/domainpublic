//
//  Health.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-06-02.
//

import Foundation

public struct Health: Codable {
    public let shoppingState: ShoppingState
    
    init(shoppingState: ShoppingState) {
        self.shoppingState = shoppingState
    }
}
