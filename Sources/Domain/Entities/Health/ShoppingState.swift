//
//  ShoppingState.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-06-02.
//

import Foundation

public enum ShoppingState: Int, Codable {
    case active = 0
    case forceControl = 1
    case paused = 2
}
