//
//  ShopApi.swift
//  Domain
//
//  Created by Carl-Johan Dahlman on 2019-10-23.
//

import Foundation


public protocol ShopApi {
    
    func createMobileSession(identificationNumber: String, createCart: Bool, receiptLabel: String?, completion: @escaping (Result<Session, ApiError>) -> Void)
    
    func create(identificationNumber: String, createCart: Bool, completion: @escaping (Result<Session, ApiError>) -> Void)
    
    func activate(sessionId: Int64, completion: @escaping (ApiError?) -> Void)
    
    func getSession(sessionId: Int64, completion: @escaping (Result<Session, ApiError>) -> Void)
    
    func getSession(referenceCode: String, completion: @escaping (Result<Session, ApiError>) -> Void)
    
    func addItem(sessionId: Int64, cartId: String, barcode: String, quantity: Double, completion: @escaping (Result<PosResponse, ApiError>) -> Void)
    
    func updateItem(sessionId: Int64, cartId: String, cartItemId: String, barcode: String, quantity: Double, completion: @escaping (Result<PosResponse, ApiError>) -> Void)
    
    func deleteItem(sessionId: Int64, cartId: String, cartItemId: String, barcode: String, completion: @escaping (Result<PosResponse, ApiError>) -> Void)
    
    func addCart(sessionId: Int64, cartId: String, completion: @escaping (Result<PosResponse, ApiError>) -> Void)
    
    func getCarts(sessionId: Int64, completion: @escaping (Result<PosResponse, ApiError>) -> Void)
    
    func deleteCart(sessionId: Int64, cartId: String, completion: @escaping (Result<PosResponse, ApiError>) -> Void)
    
    func finalize(sessionId: Int64, devicePayment: Bool, completion: @escaping (Result<PosResponse, ApiError>) -> Void)
    
    func cancel(sessionId: Int64, completion: @escaping (Result<PosResponse, ApiError>) -> Void)
    
    func clearControl(sessionId: Int64, completion: @escaping (ApiError?) -> Void)
    
    func getWorkplaces(referenceCode: String, completion: @escaping (Result<[Workplace], Error>) -> Void)
    
}
