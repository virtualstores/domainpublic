//
//  ControlPosResponse.swift
//  Domain
//
//  Created by Théodore Roos on 2020-08-10.
//

import Foundation

public struct ControlPosResponse {
    
    public let controlFlag: Int
    public let posResponse: PosResponse
    
    public init(controlFlag: Int, posResponse: PosResponse) {
        self.controlFlag = controlFlag
        self.posResponse = posResponse
    }
}

public struct CartResult {
    public var cartId: String
    public var itemResults: [ItemResult]
    
    public init(cartId: String, itemResults: [ItemResult]) {
        self.cartId = cartId
        self.itemResults = itemResults
    }
}

public struct ItemResult: Codable {
    public var id: String
    public var valid: Bool?
    
    public init(id: String, valid: Bool?) {
        self.id = id
        self.valid = valid
    }
}
