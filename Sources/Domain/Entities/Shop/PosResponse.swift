
import Foundation


public struct PosResponse {

    public let customerName: String?
    public let referenceCode: String?
    public let gateCode: String?
    public let hasControl: Bool?
    public let carts: [Cart]
	
	public init(customerName: String?, referenceCode: String?, gateCode: String?, hasControl: Bool?, carts: [Cart]) {
		self.customerName = customerName
		self.referenceCode = referenceCode
		self.gateCode = gateCode
        self.hasControl = hasControl
		self.carts = carts
	}
}
 
