//
//  Session.swift
//  Domain
//
//  Created by Carl-Johan Dahlman on 2019-10-23.
//

import Foundation


public struct Session {
    
    public enum State: Int, Codable {
        case pendingStart = 0
        case active = 1
        case finished = 2
        case rescueMode = 3
        case canceled = 4
        case timedOut = 5
        case control = 6
        case pendingPayment = 7
        case pendingReceipt = 8
        
        public var isNotActive: Bool {
            self == .finished || self == .canceled || self == .timedOut
        }
        
        public var isActive: Bool {
            !isNotActive
        }
    }
    
    public var id: Int64
    public var state: State
	
    public init(id: Int64, state: State) {
		self.id = id
        self.state = state
	}
}

