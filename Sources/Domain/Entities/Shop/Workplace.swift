//
//  Workplace.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-04-16.
//

import Foundation

public struct Workplace: Codable {
    public var id: String
    public var company: String?
    public var zipCode: Int64?
    public var address: String?
    public var city: String?
    
    public init(id: String, company: String?, zipCode: Int64?, address: String?, city: String?) {
        self.id = id
        self.company = company
        self.zipCode = zipCode
        self.address = address
        self.city = city
    }
}
