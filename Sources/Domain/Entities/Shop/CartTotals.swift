//
// CartTotalsView.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public struct CartTotals {

    public var discountAmount: Double?
    public var deliveredNowCartItemsTotalAmount: Double?
    public var saletotalAmount: Double?
    public var saletotalExclusiveVATAmount: Double?
    public var vatAmount: Double?
	
	public init(discountAmount: Double?, deliveredNowCartItemsTotalAmount: Double?, saletotalAmount: Double?, saletotalExclusiveVATAmount: Double?, vatAmount: Double?) {
		self.discountAmount = discountAmount
		self.deliveredNowCartItemsTotalAmount = deliveredNowCartItemsTotalAmount
		self.saletotalAmount = saletotalAmount
		self.saletotalExclusiveVATAmount = saletotalExclusiveVATAmount
		self.vatAmount = vatAmount
	}
}
