//
//  ShoppingListProductWrapper.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-23.
//

import Foundation

public class ShoppingListItem: Assistable, BarcodeIdentifiable {
    public let id: String?
    public let name: String
    public let quantity: Double
    public let price: Double?
    public let priceUnitOfMeasure: String?
    public let imageUrl: String?
    public let categoryName: String?
    public let brand: String?
    public let volume: String?
    public let item: Item?
    
    public static var Navigation: Navigation?
    
    public var asPointOfInterest: PointOfInterest? {
        guard let item = item else {
            return nil
        }
        
        let position = ShoppingListItem.Navigation?.getItemPosition(shelfTierId: item.shelfTierIds?.first ?? 0)
                
        let subtitle: String
        if !item.manufacturerName.isEmpty, let volume = volume, !volume.isEmpty {
            subtitle = "\(item.manufacturerName), \(volume)"
        }
        else if !item.manufacturerName.isEmpty {
            subtitle = "\(item.manufacturerName)"
        }
        else if let volume = volume, !volume.isEmpty {
            subtitle = "\(volume)"
        }
        else {
            subtitle = " "
        }
        
        return PointOfInterest(id: item.id, alternateIds: item.alternateIds?.map { $0.id } ?? [], name: item.name, subtitle: subtitle, description: "", quantity: PointOfInterest.Quantity(amount: quantity, unit: .init(fromLocalizedString: priceUnitOfMeasure ?? "st")), position: position, addedByUser: true, imageUrl: URL(string: imageUrl ?? ""), type: StorePointOfInterest.shoppingListItem)
    }
    
    enum ItemCodingKeys: String, CodingKey {
        case id
        case name
        case quantity
        case price
        case priceUnitOfMeasure
        case imageUrl
        case categoryName
        case brand
        case volume
        case item
    }
    
    public init(id: String?, name: String, quantity: Double, price: Double?, priceUnitOfMeasure: String?, imageUrl: String?, categoryName: String?, brand: String?, volume: String?, item: Item?) {
        self.id = id
        self.name = name
        self.quantity = quantity
        self.price = price
        self.priceUnitOfMeasure = priceUnitOfMeasure
        self.imageUrl = imageUrl
        self.categoryName = categoryName
        self.brand = brand
        self.volume = volume
        self.item = item
        
        super.init()
        
        self.type = .item
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ItemCodingKeys.self)
        
        id = try container.decode(String.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        quantity = try container.decode(Double.self, forKey: .quantity)
        price = try container.decode(Double.self, forKey: .price)
        priceUnitOfMeasure = try container.decode(String.self, forKey: .priceUnitOfMeasure)
        imageUrl = try container.decode(String.self, forKey: .imageUrl)
        categoryName = try container.decode(String.self, forKey: .categoryName)
        brand = try container.decode(String.self, forKey: .brand)
        volume = try container.decode(String.self, forKey: .volume)
        item = try container.decode(Item.self, forKey: .item)
        
        let superDecoder = try container.superDecoder()
        try super.init(from: superDecoder)
    }
    
    // Assistable properties
    
    override func isEqual(assistable: Assistable) -> Bool {
        if assistable is ShoppingListItem {
            let shoppingListItem = assistable as! ShoppingListItem
            if (quantity == shoppingListItem.quantity && name == shoppingListItem.name) {
                if let item = self.item {
                    if let otherItem = shoppingListItem.item {
                        return otherItem.isEqual(assistable: item)
                    }
                } else {
                    return true
                }
            }
        }

        return false
    }
    
    override func clone() -> Assistable {
        return cloneParentFields(assistable: ShoppingListItem(id: id, name: name, quantity: quantity, price: price, priceUnitOfMeasure: priceUnitOfMeasure, imageUrl: imageUrl, categoryName: categoryName, brand: brand, volume: volume, item: item))
    }
    
    // BarcodeIdentifiable properties
    func match(barcode: String) -> Bool {
        return item?.match(barcode: barcode) ?? false
    }
}
