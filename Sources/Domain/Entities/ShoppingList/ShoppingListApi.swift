//
//  ShoppingListApi.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-23.
//

import Foundation

public protocol ShoppingListApi {
    
    func getShoppingListsBy(customerId: String, completion: @escaping (Result<[ShoppingList], Error>) -> Void)
}
