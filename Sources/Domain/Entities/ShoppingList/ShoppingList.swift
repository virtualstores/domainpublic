//
//  ShoppingList.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-23.
//

import Foundation

public class ShoppingList {
    public let id: String?
    public let name: String
    public let description: String?
    public let products: [ShoppingListItem]
    
    enum ItemCodingKeys: String, CodingKey {
        case id
        case name
        case description
        case products
    }
    
    public init(id: String?, name: String, description: String?, products: [ShoppingListItem]) {
        self.id = id
        self.name = name
        self.description = description
        self.products = products
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ItemCodingKeys.self)
        
        id = try container.decode(String.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        description = try container.decode(String.self, forKey: .description)
        products = try container.decode([ShoppingListItem].self, forKey: .products)
    }
}
