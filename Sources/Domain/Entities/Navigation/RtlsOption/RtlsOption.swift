import Foundation


public struct RtlsOption {

    public let storeId: Int64
    public let width: Int
    public let height: Int
    public let startOffsetX: Int
    public let startOffsetY: Int
    public let waitTime: Int
    public let panId: String
	
	public init(storeId: Int64, width: Int, height: Int, startOffsetX: Int, startOffsetY: Int, waitTime: Int, panId: String) {
		self.storeId = storeId
		self.width = width
		self.height = height
		self.startOffsetX = startOffsetX
		self.startOffsetY = startOffsetY
		self.waitTime = waitTime
		self.panId = panId
	}
}
