import Foundation


public struct NavigationSpaceInfo {
    public let id: Int64
    public let name: String
}

// Gör till ett protokoll som Store implementerar?
public struct NavigationSpace {
    public let id: Int64
    public let name: String
    
    public let mapUrl: URL
    public let mapfence: Data
    public let navgraph: Data
    public let offsetZones: Data?
    public let mapZones: [MapZone]
    public let width: Double
    public let height: Double
    
    public let startCodes: [PositionedCode]
    public let stopCodes: [PositionedCode]
    
    public let navigation: Navigation
    
    public init(id: Int64, name: String, mapUrl: URL, mapfence: Data, navgraph: Data, offsetZones: Data? = nil, mapZones: [MapZone], width: Double, height: Double, startCodes: [PositionedCode], stopCodes: [PositionedCode], navigation: Navigation) {
        self.id = id
        self.name = name
        
        self.mapUrl = mapUrl
        self.mapfence = mapfence
        self.navgraph = navgraph
        self.mapZones = mapZones
        self.offsetZones = offsetZones
        self.width = width
        self.height = height
        
        self.startCodes = startCodes
        self.stopCodes = stopCodes
        
        self.navigation = navigation
    }
}
