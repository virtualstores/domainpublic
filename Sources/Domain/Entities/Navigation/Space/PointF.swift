//
//  PointF.swift
//  Domain
//
//  Created by Carl-Johan Dahlman on 2019-10-29.
//

import Foundation


public struct PointF {
    
    public let x: Float
    public let y: Float
	
	public init(x: Float, y: Float) {
		self.x = x
		self.y = y
	}
}
