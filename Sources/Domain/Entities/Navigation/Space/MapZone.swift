//
//  MapZone.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-06-24.
//

import Foundation
import CoreGraphics

public struct MapZone {
    public let id: Int
    public let name: String
    public let zone: [CGPoint]
    
    public init (id: Int, name: String, zone: [CGPoint]) {
        self.id = id
        self.name = name
        self.zone = zone
    }
}
