import Foundation
import CoreGraphics

public struct ItemPosition: Codable {
    
    public let x: Float
    public let y: Float
    public let offsetX: Float
    public let offsetY: Float
	
	public init(x: Float, y: Float, offsetX: Float, offsetY: Float) {
		self.x = x
		self.y = y
		self.offsetX = offsetX
		self.offsetY = offsetY
	}
    
    public var cgPointwithoutOffset: CGPoint {
        return CGPoint(x: CGFloat(x), y: CGFloat(y))
    }
}
