import Foundation


public struct ShelfTier {
    
    public let id: Int64
    public let shelfId : Int64
    public let shelfPosition: Int
	
    public init(id: Int64, shelfId: Int64, shelfPosition: Int) {
		self.id = id
        self.shelfId = shelfId
		self.shelfPosition = shelfPosition
	}
}
