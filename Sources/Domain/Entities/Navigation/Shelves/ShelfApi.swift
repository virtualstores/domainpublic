import Foundation


public protocol ShelfApi {
	
    func getShelfGroups(storeID: Int64, completion: @escaping (Result<[ShelfGroup], Error>) -> Void)
    
    func positionOnShelf(storeID: Int64, id: Int64, tierPosition: Int, barcode: String, overwrite: Bool, completion: @escaping (Error?) -> Void)
    
    func removeFromShelf(storeID: Int64, barcode: String, completion: @escaping (Error?) -> Void)
}
