//
//  Message.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2020-06-24.
//

import Foundation
import CoreGraphics

public struct Message: Equatable, Hashable {
    public enum CardType {
        case small
        case big
    }
    
    public enum ExposureType {
        case wholeMap
        case zones
    }
    
    public let id: Int64
    public let name: String
    public let duration: DateInterval
    
    public let cardType: CardType
    public let title: String
    public let description: String
    public let image: URL?
    public let exposureType: ExposureType
    // Zones is defined in degrees
    public let zones: [[CGPoint]]
    public let shelf: Shelf?
    
    public var asPointOfInterest: PointOfInterest {
        PointOfInterest(id: String(id), alternateIds: [], name: title, subtitle: description, description: "", position: shelf?.itemPosition, addedByUser: false, imageUrl: image, type: PointOfInterest.message)
    }
    
    public init(id: Int64, name: String, duration: DateInterval, cardType: CardType, title: String, description: String, image: URL?, exposureType: ExposureType, zones: [[CGPoint]], shelf: Shelf?) {
        self.id = id
        self.name = name
        self.duration = duration
        self.cardType = cardType
        self.title = title
        self.description = description
        self.image = image
        self.exposureType = exposureType
        self.zones = zones
        self.shelf = shelf
    }
    
    public static func == (lhs: Message, rhs: Message) -> Bool {
        lhs.id == rhs.id
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
    }
}
