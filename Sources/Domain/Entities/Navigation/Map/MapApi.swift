import Foundation


public protocol MapApi {
	
    func getBy(clientId: Int64, storeId: Int64, completion: @escaping (Result<Map, Error>) -> Void)
}

