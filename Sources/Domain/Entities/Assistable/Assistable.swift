//
//  Assistable.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2019-12-11.
//

import Foundation

open class Assistable: Equatable, Codable {
    public enum AssistableType: Int, Codable {
        case none
        case item
        case offer
        case news
        case reminder
        case itemGroup
        case offerGroup
        case recipe
    }
            
    public var isShoppingListItem: Bool = false
    public var isChecked: Bool = false
    public var timeChecked: Int64 = 0
    public var isSearchItem: Bool = false
    public var isMarkedForPathfinding: Bool = false
    public var isTargetForPathfinding: Bool = false
    public var isRecipeItem: Bool = false
    public var type: AssistableType = .none
    
    public func getItemPositions() -> [ItemPosition] {
        return []
    }
    
    public static func == (lhs: Assistable, rhs: Assistable) -> Bool {
        return lhs.isEqual(assistable: rhs)
    }
    
    func isEqual(assistable: Assistable) -> Bool {
        fatalError("Assistable.isEqual not implemented")
    }
    
    func cloneParentFields(assistable: Assistable) -> Assistable {
        assistable.isShoppingListItem = self.isShoppingListItem
        assistable.isChecked = self.isChecked
        assistable.timeChecked = self.timeChecked
        assistable.isSearchItem = self.isSearchItem
        assistable.isMarkedForPathfinding = self.isMarkedForPathfinding
        assistable.isTargetForPathfinding = self.isTargetForPathfinding
        assistable.isRecipeItem = self.isRecipeItem
        assistable.type = self.type
        return assistable
    }
    
    func clone() -> Assistable {
        fatalError("Assistable.clone not implemented")
    }
}
