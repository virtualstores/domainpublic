//
//  BarcodeIdentifiable.swift
//  Domain
//
//  Created by Jesper Lundqvist on 2019-12-11.
//

import Foundation

protocol BarcodeIdentifiable {
    func match(barcode: String) -> Bool
}
