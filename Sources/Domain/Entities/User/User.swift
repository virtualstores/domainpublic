//
//  User.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-23.
//

import Foundation

public class User: Codable {
    public let name: String?
    public let socialSecurity: String?
    public let memberNumber: String
    
    enum ItemCodingKeys: String, CodingKey {
        case name
        case socialSecurity
        case memberNumber
    }
    
    public init(name: String?, socialSecurity: String?, memberNumber: String) {
        self.name = name
        self.socialSecurity = socialSecurity
        self.memberNumber = memberNumber
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ItemCodingKeys.self)
        
        name = try container.decode(String.self, forKey: .name)
        socialSecurity = try container.decode(String?.self, forKey: .socialSecurity)
        memberNumber = try container.decode(String.self, forKey: .memberNumber)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: ItemCodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(socialSecurity, forKey: .socialSecurity)
        try container.encode(memberNumber, forKey: .memberNumber)
    }
}

public struct LocalUser: Codable {
    public let fullName: String
    public let memberNumber: String
    public let socialSecurity: String
    public let password: String?
    public let externalCustomerId: String?
    public let controlLevel: Int64
    public let successCount: Int64
    public let dateCreated: Date?
    public let lastLogin: Date?
    public let controlFlag: Int64
    public let id: Int64
}
