//
//  UserApi.swift
//  Domain
//
//  Created by Gustav Larson on 2020-01-23.
//

import Foundation

public protocol UserApi {
    func verifyUser(username: String, password: String, completion: @escaping (Result<LocalUser, Error>) -> Void)
}
