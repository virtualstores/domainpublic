//
//  Utils.swift
//  Domain
//
//  Created by Carl-Johan Dahlman on 2019-10-25.
//

import Foundation


private let asyncQueue = DispatchQueue.global(qos: .userInitiated)
public func async(queue: DispatchQueue? = nil, closure: @escaping () -> ()) {
    (queue ?? asyncQueue).async(execute: closure)
}


private let syncQueue = DispatchQueue(label: "com.virtualstores.domain", qos: DispatchQoS.default, attributes: .concurrent, autoreleaseFrequency: DispatchQueue.AutoreleaseFrequency.inherit, target: nil)
@discardableResult
public func sync <T> (queue: DispatchQueue? = nil, closure: () throws -> T) rethrows -> T {
	try (queue ?? syncQueue).sync(execute: closure)
}
