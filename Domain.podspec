Pod::Spec.new do |spec|
  spec.name         = "Domain"
  spec.version      = "1.6.3"
  spec.summary      = "Domain logic"
  spec.description  = "All the bussiness logic lies here"
  spec.homepage     = "http://virtualstores.se"
  spec.license      = "MIT"
  spec.author             = { "cj" => "carljohan.dahlman@virtualstores.se" }
  spec.platform     = :ios
  spec.ios.deployment_target = "11.0"
  spec.swift_version = '5.0'
  spec.source       = { :git => "https://bitbucket.org/virtualstores/DomainPublic.git", :tag => spec.version }

  spec.source_files  = "Sources", "Sources/**/*.{h,m,swift}"
  spec.exclude_files = "Classes/Exclude"
end
